<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//*********************Define Route***************************************/
Route::get('/','Home@index');
Route::get('home','Home@index');
Route::get('about-us','Home@about_us');
Route::get('clinic','Home@about_clinic');
Route::get('male-sex-problems', 'Home@male_sexual_problem');
Route::get('female-sex-problems', 'Home@female_sexual_problem');
Route::get('male-factor-infertility', 'Home@male_factor_infertility');
Route::get('female-factor-infertility', 'Home@female_factor_infertility');
Route::get('common-diseases', 'Home@common_diseases');
Route::get('gallary/achievements', 'Home@achievements');
Route::get('premeture-ejeculation', 'Home@premeture_ejeculation');
Route::get('erectile-dysfunction', 'Home@erectile_dysfunction');
Route::get('nightfall', 'Home@night_fall');
Route::get('loss-of-libido', 'Home@loss_of_libido');
Route::get('dhat', 'Home@dhat');
Route::get('low-testosterone', 'Home@low_testosterone');
Route::get('infertility', 'Home@infertility');
Route::get('semen-in-urine', 'Home@semen_in_urine');
Route::get('penis-treatments', 'Home@penis_treatments');
Route::get('small-breasts', 'Home@small_breasts');
Route::get('lack-of-sex-desire', 'Home@lack_of_sex_desire');
Route::get('absent-orgasm', 'Home@absent_orgasm');
Route::get('painfull-intercourse', 'Home@painfull_intercourse');
Route::get('women-infertility', 'Home@women_infertility');
Route::get('women-obesity', 'Home@women_obesity');
Route::get('low-sperm-count', 'Home@low_sperm_count');
Route::get('nil-sperm-count', 'Home@nil_sperm_count');
Route::get('dead-sperm', 'Home@dead_sperm');
Route::get('female-infertility', 'Home@female_infertility');
Route::get('thyroid-disorder', 'Home@thyroid_disorder');
Route::get('recurrent-abortion', 'Home@recurrent_abortion');
Route::get('infrastructure', 'Home@Infrastructure');
Route::get('piles', 'Home@Piles');
Route::get('menstrual-disorder', 'Home@Menstrual_disorder');
Route::get('diabetes', 'Home@Diabetes');
Route::get('hairfall', 'Home@Hairfall');
Route::get('hairgreying', 'Home@Hairgreying');
Route::get('weight-gain', 'Home@Weight_gain');
Route::get('obesity-problem', 'Home@Obesity_problem');
Route::get('pimples', 'Home@Pimples');
Route::get('contact', 'Home@contact_us');
Route::get('faq', 'Home@faq');
Route::get('videos', 'Home@videos');
Route::get('certifications', 'Home@Certificates');
Route::get('golden-moments', 'Home@gallary');
Route::get('dr.ruby_siddiqui', 'Home@about_dr_siddiqui');
Route::post('patient/mail', 'Home@quick_contact');
Route::get('medicine', 'Home@online_medcine');
Route::get('marriage-counselling', 'Home@marriage_counselling');

/* Artisan Call */
Route::get('call', 'Composer@call');
Route::get('migrate', 'Composer@migrate');

Route::get('artisan/command', 'Composer@artisan_view');
Route::post('get-artisan-command', 'Composer@artisan_command')->name('artisan.command');

Route::get('command', function () {
	/* php artisan migrate */
    \Artisan::call('make:migration create_password_resets_table');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/redirect', 'SocialAuthGoogleController@redirect');
Route::get('/callback', 'SocialAuthGoogleController@callback');

Route::match(['GET', 'POST'], 'upload/photos', 'PhotoController@upload')->name('uploadPhoto');
Route::get('photos', 'PhotoController@list')->name('photos');
Route::get('photo/delete/{id}', 'PhotoController@delete')->name('photo-delete');
