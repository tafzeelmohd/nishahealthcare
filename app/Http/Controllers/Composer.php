<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Database\Eloquent\Model;
use DB;
use File;
use Artisan;
use Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Http\Requests;


class Composer extends BaseController
{

	public function call()
	{	
		// Dump Backup file
		Artisan::call("migrate");
	}

	public function migrate()
	{
		Artisan::call("migrate");
	}

	public function artisan_view()
	{
		return view('pages/artisan-call');
	}

	public function artisan_command(Request $req)
	{
		$command = $req->input('command_line');

		Artisan::call('migrate');
	}

}