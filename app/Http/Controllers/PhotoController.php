<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Photos;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use File;

class PhotoController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function upload(Request $request) {

    	switch ($request->method()) {

    		case 'GET':
    			return view('pages.admin.photo-upload');
    			break;
    		
    		case 'POST':

			    $this->validate(request(),[
                'title' => 'max:200',
                'image' => 'required | image | mimes:jpeg,png,jpg,gif,svg',
            ]);

            $photos = new Photos;
            $hashid = str_random(10);

			if($request->hasFile('image')) {
                $image = 'photo-'.time().$hashid.'.'.request()->image->getClientOriginalExtension();
                $data = array([
                    'image' => $image
                ]);

                request()->image->move(public_path('front/upload/gallary'), $image);
            }

            $photos->title = $request->title;
            $photos->image = $image;

            if($photos->save()) {
            	return redirect()->back()->with('success','Image Upload successfully Upload');
            } else {
            	return redirect()->back()->with('error','Something went weong try again');
            }

    		break;
    	}
    }

    public function list(Request $request) {
        $data['photos'] = Photos::paginate(10);
        return view('pages.admin.photos-list', $data);
    }


    public function delete(Request $request) {

        $photo = Photos::find($request->id);
        if(!empty($photo)) {
            $response = Photos::where('id', $request->id)->delete();

             if(File::exists(public_path('front/upload/gallary'), $photo->image))   {
                File::delete(public_path('front/upload/gallary'), $photo->image);
             }

             if($response) {
                return redirect()->back()->with('success','Image Delete successfully');
            } else {
                return redirect()->back()->with('error','Something went weong try again');
            }
        }

    }
}
