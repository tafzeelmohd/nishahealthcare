<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Session;
use DB;
use App\Tracker;
use App\Photos;

class Home extends BaseController
{
	public function index() {

        $data['photos'] = Photos::where('status', '1')->orderBy('id', 'desc')->take(6)->get();
        return view('pages/home', $data);
    }

    public function about_us(){
    	return view('pages/about-us');
    }

    public function about_clinic(){
    	return view('pages/about-clinic');
    }

    public function male_sexual_problem(){
    	return view('pages/male-sexual-problem');
    }

    public function female_sexual_problem(){
    	return view('pages/female-sexual-problem');
    }

    public function male_factor_infertility(){
    	return view('pages/male-factor-infertility');
    }

    public function female_factor_infertility(){
    	return view('pages/female-factor-infertility');
    }

    public function common_diseases(){
        return view('pages/common-diseases');
    }

    public function achievements(){
        return view('pages/achievements');
    }

    public function premeture_ejeculation(){
        return view('pages/premeture-ejeculation');
    }

    public function erectile_dysfunction(){
        return view('pages/erectile-dysfunction');
    }
	
    public function night_fall(){
        return view('pages/nightfall');
    }

    public function loss_of_libido(){
        return view('pages/loss-of-libido');
    }

    public function dhat(){
        return view('pages/dhat');
    }

    public function low_testosterone(){
        return view('pages/low-testosterone');
    }

    public function infertility(){
        return view('pages/Infertility');
    }

    public function semen_in_urine(){
        return view('pages/semen-in-urine');
    }

    public function penis_treatments(){
        return view('pages/penis-treatments');
    }

    public function small_breasts(){
        return view('pages/small-breast');
    }

    public function lack_of_sex_desire(){
        return view('pages/lack-of-sex-desire');
    }

    public function absent_orgasm(){
        return view('pages/absent-orgasm');
    }

    public function painfull_intercourse(){
        return view('pages/painfull-intercourse');
    }

    public function women_infertility(){
        return view('pages/women-infertility');
    }

    public function women_obesity(){
        return view('pages/obesity');
    }

    public function low_sperm_count(){
        return view('pages/low-sperm-count');
    }

    public function nil_sperm_count(){
        return view('pages/nil-sperm-count');
    }

    public function dead_sperm(){
        return view('pages/dead-sperm');
    }

    public function female_infertility(){
        return view('pages/female-fertility');
    }

    public function thyroid_disorder(){
        return view('pages/thyroid-disorder-Infertility');
    }

    public function recurrent_abortion(){
        return view('pages/recurrent-abortions');
    }

    public function contact_us(){
        return view('pages/contact-us');
    }

    public function gallary(){
        $data['photos'] = Photos::where('status', '1')->orderBy('id', 'desc')->paginate(50);
        return view('pages/gallary', $data);
    }

    public function Infrastructure(){
        return view('pages/Infrastructure');
    }

    public function piles(){
        return view('pages/piles');
    }

    public function Menstrual_disorder(){
        return view('pages/menstrual-disorder');
    }

    public function Diabetes(){
        return view('pages/diabetes');
    }

    public function Hairfall(){
        return view('pages/hairfall');
    }

    public function Hairgreying(){
        return view('pages/hairgraying');
    }

    public function Weight_gain(){
        return view('pages/weight-gain');
    }

    public function Obesity_problem(){
        return view('pages/obesity-problem');
    }

    public function Pimples(){
        return view('pages/pimples');
    }

    public function Certificates(){
        return view('pages/certificates');
    }

    public function faq(){
        return view('pages/faq');
    }

    public function videos(){
        return view('pages/videos');
    }

    public function about_dr_siddiqui(){
        return view('pages/doctor-panel');
    }

    public function marriage_counselling()
    {
         return view('pages/marriage-counselling');
    }

    public function quick_contact(Request $request)
    {
        $name = $request->input('name');
        $contact = $request->input('contact');
        $problem = $request->input('problem');

        $to = 'nishahealthcare0786@gmail.com';
        $subject = 'Patient Enquiry';
        $from = "someonelse@example.com";

        // $headers =  'MIME-Version: 1.0';
        // $headers .= 'From: Paient Query <info@address.com>';
        // $headers .= 'Content-type: text/html; charset=iso-8859-1';

        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
         
        // Create email headers
        $headers .= 'From: Paient Query <www.mssiddiqui.com>'."\r\n".
            'Reply-To: '.$to."\r\n" .
            'X-Mailer: PHP/' . phpversion();

        $message = '<html><body>';
        $message .= '<h3 style="color:#d71d1d;">Patient Name:'.$name.'</h3>';
        $message .= '<h3 style="color:#d71d1d;">Patient Contact No:'.$contact.'</h3>';
        $message .= '<h3 style="color:#d71d1d;">Query From: www.mssiddiqui.com</h3>';
        $message .= '<p style="color:#080;font-size:18px;">Patient Query:'.$problem.'</p>';
        $message .= '</body></html>';

        $res = mail($to,$subject,$message,$headers);

        if($res)
        {
            return redirect()->back();
        }
    }

    
    public function online_medcine(){
        return view('pages/online-medine');
    }
    
    public function contact(Request $request)
    {
        $name = $request->input('name');
        $contact = $request->input('contact');
        $problem = $request->input('problem');

        $to = $request->input('email');
        $subject = 'Patient Enquiry';
        $from = "someonelse@example.com";

        // $headers =  'MIME-Version: 1.0';
        // $headers .= 'From: Paient Query <info@address.com>';
        // $headers .= 'Content-type: text/html; charset=iso-8859-1';

        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
         
        // Create email headers
        $headers .= 'From: Paient Query <www.mssiddiqui.com>'."\r\n".
            'Reply-To: '.$to."\r\n" .
            'X-Mailer: PHP/' . phpversion();

        $message = '<html><body>';
        $message .= '<h3 style="color:#d71d1d;">Patient Name:'.$name.'</h3>';
        $message .= '<h3 style="color:#d71d1d;">Patient Contact No:'.$contact.'</h3>';
        $message .= '<h3 style="color:#d71d1d;">Query From: www.mssiddiqui.com</h3>';
        $message .= '<p style="color:#080;font-size:18px;">Patient Query:'.$problem.'</p>';
        $message .= '</body></html>';

        $res = mail($to,$subject,$message,$headers);

        if($res)
        {
            return redirect()->back();
        }
    }
}