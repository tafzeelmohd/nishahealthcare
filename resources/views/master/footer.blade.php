<?php  App\Tracker::hit();
$counter =  App\Tracker::count();
?>
<!-- footer-area -->
<footer class="footer-section">
    <div class="container">
        <div class="footer-content pt-50">
            <div class="row">
                <div class="col-xl-4 col-lg-4 mb-10">
                    <div class="">
                        <div class="footer-logo">
                            <a href="{{''}}">
                              <img src="{!! asset('front/img/logo/ms.png') !!}" width="150" height="60" class="img-fluid" alt=""></a>
                        </div>
                        <div class="footer-text degree">
                            <p>Dr. M.S. Siddiqui</p>
                                <ul>
                                   <li>M.D.(AM),BUMS,CHFE,DAP,PGDYT,FIANT</li>
                                   <li>Member of indian association of sexology</li>
                                   <li> Member of south asian society for sexual medicine</li>
                                   <li> fellow of international society for sexual medicine for nedarland</li>
                                   <li>fellow of indian academy of natural therapeutic</li>
                                </ul>
                           </div>
                        </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 mb-30">
                    <div class="footer-widget">
                        <div class="footer-widget-heading">
                            <h3>Useful Links</h3>
                        </div>
                        <ul>
                            <li><a href="{{''}}">Home</a></li>
                            <li><a href="{{'about-us'}}">About us</a></li>
                            <li><a href="{{'golden-moments'}}">Gallary</a></li>
                            <li><a href="{{'clinik'}}">About Clinic</a></li>
                            <li><a href="{{'contact'}}">Contact us</a></li>
                            <li><a href="{{'online/medicine'}}">Medicine</a></li>
                            <li><a href="{{'faq'}}">Faq's</a></li>
                            <li><a href="https://tawk.to/chat/5bc8c46fb9993f2ada14b752/default/?$_tawk_sk=5c4200408214824805142893&amp;$_tawk_tk=54d95a0006a64d5a9891e9d9adc238fa&amp;v=636" target="_blank">Online Help</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 mb-50">
                  <div class="footer-widget">
                        <div class="footer-widget-heading">
                            <h3>Contact Us</h3>
                        </div>
                        <p>
                            <span>Nisha Health Care Clinic</span>
                            <span>Ghosiyana, Station Road ( Near - Ultra Pathalogy)
                            Civil Lines Barabanki-225001(U.P.)</span>
                          <span><i class="far fa-envelope-open">&nbsp;&nbsp;</i>nishahealthcare0786@gmail.com</span>
                          <span><i class="fas fa-phone">&nbsp;&nbsp;</i>+91-9839467283</span>
                      </p>
                    </div>
                       <div class="footer-social-icon">
                            <a href="https://www.facebook.com/mohd.sharif.359" target="_blank"><i class="fab fa-facebook-f facebook-bg"></i></a>
                            <a href="https://twitter.com/DrMSSiddiqui1" target="_blank"><i class="fab fa-twitter twitter-bg"></i></a>
                            <a href="https://www.youtube.com/channel/UCpwYNtnRg3q1osCmFmlvBHQ" target="_blank"><i class="fab fa-youtube youtube-bg"></i></a>
                  </div></div>
                </div>
            </div>
        </div>
    </div>

    <div class="copyright-area">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 col-lg-5 text-center text-lg-left">
                    <div class="copyright-text">
                        <p>Copyright &copy; 2018, All Right Reserved <a href="JavaScript:;">Nisha Health Care</a></p>
                    </div>
                </div>
                <div class="col-xs-2 col-lg-2 text-center">
                    <div class="copyright-text">
                        <p><!-- {{$counter }} --></p>
                    </div>
                </div>
                <div class="col-xl-5 col-lg-5 d-none d-lg-block text-right"><div class="copyright-text">
                    <p>Design & Developed By <a href="//{{'www.tafzeelmohd.online'}}" target="_blank">Tafzeel Mohammad</a></p></div>
                </div>
            </div>
        </div>
    </div>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<a href="https://api.whatsapp.com/send?phone=919839467283&text=Hello%20dr.%20siddiqui%20i%20need%20your%20help." class="float" target="_blank">
<i class="fa fa-whatsapp my-float"></i>
</a>
</footer>

<!-- Optional JavaScript -->

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="{!! asset('front/js/popper.min.js') !!}"></script>
<script src="{!! asset('front/js/bootstrap.min.js') !!}"></script>
<script src="{!! asset('front/js/slick.min.js') !!}"></script>
<script src="{!! asset('front/js/isotope.pkgd.min.js') !!}"></script>
<script src="{!! asset('front/js/imagesloaded.pkgd.min.js') !!}"></script>
<!-- <script src="{!! asset('front/js/jquery.waypoints.min.js') !!}"></script> -->
<script src="{!! asset('front/js/jquery.counterup.min.js') !!}"></script>
<script src="{!! asset('front/js/owl.carousel.min.js') !!}"></script>
<script src="{!! asset('front/js/jquery.meanmenu.min.js') !!}"></script>
<script src="{!! asset('front/js/jquery.magnific-popup.min.js') !!}"></script>
<script src="{!! asset('front/js/scrollup.min.js') !!}"></script>
<script src="{!! asset('front/js/main.js') !!}"></script>


<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5bc8c46fb9993f2ada14b752/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
</script>


<script>
    $(document).ready(function() {
    'use strict';  
    var prealoaderOption = $(window);
    prealoaderOption.on("load", function () {
        var preloader = jQuery('.spinner');
        var preloaderArea = jQuery('.preloader-area');
        preloader.fadeOut('fast');
        preloaderArea.fadeOut('fast');
    });
});
</script>

<style type="text/css">
#scrollUp {
position: absolute;
right: 82px;
bottom: 63px;
}
.float{
position:fixed;
width:60px;
height:60px;
bottom:90px;
right:10px;
background-color:#25d366;
color:#FFF;
border-radius:50px;
text-align:center;
font-size:30px;
box-shadow: 2px 2px 3px #999;
z-index:100;
}

.my-float{ margin-top:16px;}
.float:hover { color: #fff; }
.footer-text.degree p {
    padding: 0;
    margin: 0;
    line-height: 20px;
    font-size: 12px;
}.footer-text.degree ul li {
    color: #fff;
    list-style-type: square;
    font-size: 12px;
    line-height: 20px;
}

.footer-text.degree ul {
    padding-left: 14px;
}

.footer-text.degree p {
    font-size: 14px;
    /* padding-left: 13px; */
    margin-bottom: 5px;
}.footer-text p span {
    display: block;
    line-height: 25px;
}

.footer-text {
    margin-top: 15px;
}
iframe#xAVQlaO-1548407273904
{
    height: 30px !important;
    min-height: 30px !important;
}
</style>
</body>
</html>