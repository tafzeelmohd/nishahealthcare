<!doctype html>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" href="{!! asset('front/img/favicon.png') !!}">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{!! asset('front/css/bootstrap.min.css') !!}">
<link rel="stylesheet" href="{!! asset('front/css/fontawesome.min.css') !!}">
<link rel="stylesheet" href="{!! asset('front/css/icofont.min.css') !!}">
<link rel="stylesheet" href="{!! asset('front/css/linearicons.css') !!}">
<link rel="stylesheet" href="{!! asset('front/css/slick.css') !!}">
<link rel="stylesheet" href="{!! asset('front/css/owl.carousel.min.css') !!}">
<link rel="stylesheet" href="{!! asset('front/css/animate.min.css') !!}">
<link rel="stylesheet" href="{!! asset('front/css/magnific-popup.css') !!}">
<link rel="stylesheet" href="{!! asset('front/css/meanmenu.css') !!}">
<link rel="stylesheet" href="{!! asset('front/css/default.css') !!}">
<link rel="stylesheet" href="{!! asset('front/css/style.css') !!}">
<link rel="stylesheet" href="{!! asset('front/css/responsive.css') !!}">
<script src="{!! asset('front/js/vendor/jquery-1.12.4.min.js') !!}"></script>

<title>Dr. M.S Siddiqui | Sexologist | General Physician</title>

</head>
<body>

<!-- <div class="preloader-area">
    <div class="spinner">
        <div class="hearbeat animated pulse">
            <i class="fas fa-heartbeat"></i>
        </div>
    </div>
</div> -->

<!-- header-area -->
<header>
    <div class="header-top pink-bg">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-8 col-lg-8 col-md-8 text-center text-md-left">
                    <div class="header-top-cta">
                        <!-- <span><i class="fas fa-map-marker-alt"></i>Ghosiyana, Station Road, Barabanki</span> -->
                        <span><i class="far fa-envelope-open"></i>nishahealthcare0786@gmail.com</span>
                        <span><i class="fas fa-phone"></i>+91-9839467283</span>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 text-center text-md-right">
                    <div class="top-social-links">
                        <span>Follow us:</span>
                        <a href="https://www.facebook.com/mohd.sharif.359" target="_blank"><i class="fab fa-facebook-f"></i></a>
                        <a href="https://twitter.com/DrMSSiddiqui1" target="_blank"><i class="fab fa-twitter"></i></a>
                        <a href="https://www.youtube.com/channel/UCpwYNtnRg3q1osCmFmlvBHQ" target="_blank"><i class="fab fa-youtube"></i></a>
                        &nbsp;&nbsp;
                          @guest
                        <a href="{{route('login')}}">Login</a>
                          @else
                          <a class="" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                        @endguest
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div id="sticky-header" class="menu-area">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-5">
                    <div class="logo">
                        <a href="{{url('/')}}"><img src="{!! asset('front/img/logo/ms.png') !!}" width="230" ></a>
                    </div>
                </div>
                <div class="col-xl-10 col-lg-10 text-right">
                    <div class="main-menu">
                        <nav id="mobile-menu">
                            <ul>
                                <li><a href="{{url('home')}}" class="menu-active">Home</a>
                               
                                </li>
                                <li><a href="javascript:;">About Us<i class="icofont-simple-down"></i></a>
                                  <ul class="sub-menu">
                                        <li><a href="{{url('about-us')}}">Dr. Siddiqui</a></li>
                                         <li><a href="{{url('dr.ruby_siddiqui')}}">Dr. Ruby Siddiqui</a></li>
                                        <li><a href="{{url('clinic')}}">About Our Clinic</a></li>
                                        <li><a href="{{url('infrastructure')}}">Infrastructure</a></li>
                                    </ul>
                                </li>
                                <li><a href="services.html">Disease<i class="icofont-simple-down"></i></a>
                                    <ul class="sub-menu">
                                        <li><a href="{{url('male-sex-problems')}}">Male Sexual Problem</a></li>
                                        <li><a href="{{url('female-sex-problems')}}">Female Sexual Problem</a></li>
                                        <li><a href="{{url('male-factor-infertility')}}">Male Factor Infertility</a></li>
                                        <li><a href="{{url('female-factor-infertility')}}">Female Factor Infertility</a></li>
                                        <li><a href="{{url('common-diseases')}}">Common Dieases</a></li>
                                        <li><a href="{{url('marriage-counselling')}}">Marriage counselling</a></li>
                                    </ul>
                                </li>
                                <li><a href="JavaScript:;">Ask online<i class="icofont-simple-down"></i></a>
                                    <ul class="sub-menu">
                                        <li><a href="https://tawk.to/chat/5bc8c46fb9993f2ada14b752/default/?$_tawk_sk=5c4200408214824805142893&$_tawk_tk=54d95a0006a64d5a9891e9d9adc238fa&v=636" target="_blank">Get Online Consultation</a></li>
                                        <li><a href="{{url('medicine')}}">Medicine By Post</a></li>
                                    </ul>
                                </li>
                                <li><a href="JavaScript:;">Gallary<i class="icofont-simple-down"></i></a>
                                   <ul class="sub-menu">
                                        <li><a href="{{url('golden-moments')}}">Golden Moments</a></li>
                                        <li><a href="{{url('certifications')}}">Certificates</a></li>
                                        <li><a href="{{url('videos')}}">Videos</a></li>
                                    </ul>
                                </li>
                                 
                                </li>
                                 <li><a href="{{url('faq')}}">Faq</a>
                                <li><a href="{{url('contact')}}">Contact Us</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-12">
                    <div class="mobile-menu"></div>
                </div>
            </div>
            <div class="offcanvas-menu">
                <span class="menu-close"><i class="fas fa-times"></i></span>
                <form action="#">
                    <input type="text" placeholder="Search..">
                    <button><i class="fa fa-search"></i></button>
                </form>
                <ul>
                    <li><a href="javascript:;">Home</a></li>
                    <li><a href="javascript:;">About</a></li>
                    <li><a href="javascript:;">Services</a></li>
                    <li><a href="javascript:;">Portfolio</a></li>
                    <li><a href="javascript:;">Blog</a></li>
                    <li><a href="javascript:;">Contact</a></li>
                </ul>
                <div class="side-social">
                    <a href="javascript:;"><i class="fab fa-facebook-f"></i></a>
                    <a href="javascript:;"><i class="fab fa-twitter"></i></a>
                    <a href="javascript:;"><i class="fab fa-google-plus-g"></i></a>
                    <a href="javascript:;"><i class="fab fa-linkedin-in"></i></a>
                    <a href="javascript:;"><i class="fab fa-instagram"></i></a>
                </div>
            </div>
            <div class="offcanvas-overly">
                
            </div>
        </div>
    </div>
</header>




