@extends('master.default')

@section('content')



<main>

    <!-- breadcrumb-area -->

    <section class="breadcrumb-area blue-bg">

        <div class="container">

            <div class="row">

                <div class="col-xl-6 col-md-6 mb-15 mt-15">

                    <div class="breadcrumb-title">

                        <h3>Weight Gain</h3>

                    </div>

                </div>

                <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">

                    <div class="breadcrumb">

                        <ul>

                            <li><a href="#">Home</a></li>

                            <li><a href="#">Common Diseases</a></li>

                            <li><a href="#">Weight Gain</a></li>

                        </ul>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <!--Premeture Ejeculation Problems -->



    <div class="services-area pt-50 pb-65">

    <div class="container">

        <div class="row">

            <div class="col-xl-8 col-lg-8">

                <div class="row">

                    <div class="col-xl-12">

                        <div class="service-details mb-20">

                            <img class="mb-30" src="{!! asset('front/img/common-problems/weight-gain-deatil.jpg') !!}" alt="">

                            <br>

                            <h3>Weight Gain ?</h3>

                            <div class="mid-bar"></div>

                            

                            <p><strong>Weight Reduction - Obesity:</strong></p>



                            <p>Obesity is a medical condition in which excess body fat has accumulated to the extent that it may have an adverse effect on health, leading to reduced life expectancy and/or increased health problems.</p>



                            <p>Obesity increases the likelihood of various diseases, particularly heart disease, type 2 diabetes, obstructive sleep apnea, certain types of cancer, and osteoarthritis.Obesity is most commonly caused by a combination of excessive food energy intake, lack of physical activity, and genetic susceptibility, although a few cases are caused primarily by genes, endocrine disorders, medications or psychiatric illness. Evidence to support the view that some obese people eat little yet gain weight due to a slow metabolism is limited; on average obese people have a greater energy expenditure than their thin counterparts due to the energy required to maintain an increased body mass.</p>



                            <p><strong>What do we do about it? </strong></p>

                            

                            <p>We at Noble ensure that you do not hail any ailments of sexual disorders due to Diabetes and continue living your love life to it full extent.</p>

                            

                            <p>We use exclusive herbl extracts to formulate our weight loss medicine and we make it completely customized to suit your body.</p>



                            </div>

                        </div>

                    </div>

                </div>

          

                 <div class="col-xl-4 col-lg-4">
                    @include('pages/latest-news')
                    @include('pages/related-diseas')
                    @include('pages/quick-contact')
                </div>


        </div>

    </div>

    </div>



</main>

@endsection