@extends('master.default')
@section('content')

<main>
    <!-- breadcrumb-area -->
    <section class="breadcrumb-area blue-bg">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-6 mb-15 mt-15">
                    <div class="breadcrumb-title">
                        <h3>Infertility</h3>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Womens Sex Problems</a></li>
                            <li><a href="#">Infertility</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Painfull Intercourse Problems -->

    <div class="services-area pt-50 pb-65">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 col-lg-8">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="service-details mb-20">
                            <img class="mb-30" src="{!! asset('front/img/women-problems/infertility-details.jpg') !!}" alt="">

                            <h3>Causes:</h3>
                              <div class="mid-bar"></div>
                                <div class="service-details mt-10 mb-40">
                                <p>To become pregnant, each of these factors is essential.</p>

                                <ul class="list2">
                                  <li><strong>You need to ovulate</strong> Achieving pregnancy requires that your ovaries produce and release an egg, a process known as ovulation. Your doctor can help evaluate your menstrual cycles and confirm ovulation.</li>
                                  <li><strong>Your partner needs sperm</strong> For most couples, this isn't a problem unless your partner has a history of illness or surgery. Your doctor can run some simple tests to evaluate the health of your partner's sperm.</li>
                                  <li><strong>You need to have regular intercourse</strong> You need to have regular sexual intercourse during your fertile time. Your doctor can help you better understand when you're most fertile during your cycle.</li>
                                  <li><strong>You need to have open fallopian tubes and a normal uterus</strong> The egg and sperm meet in the fallopian tubes, and the pregnancy needs a healthy place to grow.</li>
                                </ul>

                                <br>
                                <p>For pregnancy to occur, every part of the complex human reproduction process has to take place just right. The steps in this process are as follows:</p>

                              <ul class="list2">
                                  <li>One of the two ovaries releases a mature egg.</li>
                                  <li>The egg is picked up by the fallopian tube.</li>
                                  <li>Sperm swim up the cervix, through the uterus and into the fallopian tube to reach the egg for fertilization.</li>
                                  <li>The fertilized egg travels down the fallopian tube to the uterus.</li>
                                  <li>The fertilized egg implants and grows in the uterus.</li>
                                </ul>
                                <br>
                                <p>In women, a number of factors can disrupt this process at any step. Female infertility is caused by one or more of these factors.</p>
                                <br>
                                <h3><strong>Ovulation disorders:</strong></h3>
                                <div class="mid-bar"></div>
                                <p>Ovulation disorders, meaning you ovulate infrequently or not at all, account for infertility in about 25 percent of infertile couples. These can be caused by flaws in the regulation of reproductive hormones by the hypothalamus or the pituitary gland, or by problems in the ovary itself.</p>
                                
                                <ul class="list2">
                                  <li><strong>Polycystic ovary syndrome (PCOS).</strong> In PCOS, complex changes occur in the hypothalamus, pituitary gland and ovaries, resulting in a hormone imbalance, which affects ovulation. PCOS is associated with insulin resistance and obesity, abnormal hair growth on the face or body, and acne. It's the most common cause of female infertility.Achieving pregnancy requires that your ovaries produce and release an egg, a process known as ovulation. Your doctor can help evaluate your menstrual cycles and confirm ovulation.</li>

                                  <li><strong>Hypothalamic dysfunction</strong> The two hormones responsible for stimulating ovulation each month — follicle-stimulating hormone (FSH) and luteinizing hormone (LH) — are produced by the pituitary gland in a specific pattern during the menstrual cycle. Excess physical or emotional stress, a very high or very low body weight, or a recent substantial weight gain or loss can disrupt this pattern and affect ovulation. The main sign of this problem is irregular or absent periods.</li>

                                  <li>Premature ovarian insufficiency This disorder is usually caused by an autoimmune response where your body mistakenly attacks ovarian tissues or by premature loss of eggs from your ovary due to genetic problems or environmental insults such as chemotherapy. It results in the loss of the ability to produce eggs by the ovary, as well as a decreased estrogen production under the age of 40.</li>

                                  <li>Too much prolactin Less commonly, the pituitary gland can cause excess production of prolactin (hyperprolactinemia), which reduces estrogen production and may cause infertility. Most commonly this is due to a problem in the pituitary gland, but it can also be related to medications you're taking for another disease.</li>
                                </ul>
                                <br><br>

                                <h3><strong>Risk factors:</strong></h3>
                                <div class="mid-bar"></div>
                                
                                <p>Certain factors may put you at higher risk of infertility, including:</p>
                                <ul class="list2">
                                  <li><strong>Age</strong> With increasing age, the quality and quantity of a woman's eggs begin to decline. In the mid-30s, the rate of follicle loss accelerates, resulting in fewer and poorer quality eggs, making conception more challenging and increasing the risk of miscarriage.</li>

                                  <li><strong>Smoking</strong> Besides damaging your cervix and fallopian tubes, smoking increases your risk of miscarriage and ectopic pregnancy. It's also thought to age your ovaries and deplete your eggs prematurely, reducing your ability to get pregnant. Stop smoking before beginning fertility treatment.</li>

                                  <li><strong>Weight</strong> If you're overweight or significantly underweight, it may hinder normal ovulation. Getting to a healthy body mass index (BMI) has been shown to increase the frequency of ovulation and likelihood of pregnancy.</li>

                                  <li><strong>Sexual history</strong> Sexually transmitted infections such as chlamydia and gonorrhea can cause fallopian tube damage. Having unprotected intercourse with multiple partners increases your chances of contracting a sexually transmitted disease (STD) that may cause fertility problems later.</li>

                                  <li><strong>Alcohol</strong> Heavy drinking is associated with an increased risk of ovulation disorders and endometriosis.</li>

                                </ul>


                              </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-lg-4">
                @include('pages/related-diseas');
                @include('pages/quick-contact');
            </div>
        </div>
    </div>
    </div>

</main>
@endsection