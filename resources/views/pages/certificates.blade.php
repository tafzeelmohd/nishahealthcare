@extends('master.default')
@section('content')

<main>
    <!-- breadcrumb-area -->
    <section class="breadcrumb-area blue-bg">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-6 mb-15 mt-15">
                    <div class="breadcrumb-title">
                        <h3>Degree & Certificates</h3>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="{{'home'}}">Home</a></li>
                            <li><a href="javascript:;">Gallary</a></li>
                            <li><a class="active" href="{{'certificates'}}">Degree & Certificates</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Painfull Intercourse Problems -->

       <section class="portfolio-area grey-bg pt-50 pb-40">
            <div class="container">
                <div class="row portfolio-active">
                    <div class="col-xl-3 col-lg-3 col-md-6 mb-30 grid-item cat-four cat-two">
                        <div class="portfolio-wrapper">
                            <div class="portfolio-thumb portfolio-popup">
                                <img src="{!! asset('front/img/certificates/1.jpg') !!}" class="img-fluid" alt="">
                                <a href="{!! asset('front/img/certificates/1.jpg') !!}" class="view"><i class="icofont-plus"></i></a>
                            </div>
                           <!--  <div class="portfolio-content">
                                <h5><a href="#">Finance Consultancy</a></h5>
                                <span>supporting text</span>
                            </div> -->
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6 mb-30 grid-item cat-one cat-three">
                        <div class="portfolio-wrapper">
                            <div class="portfolio-thumb portfolio-popup">
                                <img src="{!! asset('front/img/certificates/2.jpg') !!}" class="img-fluid" alt="">
                                <a href="{!! asset('front/img/certificates/2.jpg') !!}" class="view"><i class="icofont-plus"></i></a>
                            </div>
                           <!--  <div class="portfolio-content">
                                <h5><a href="#">Finance Consultancy</a></h5>
                                <span>supporting text</span>
                            </div> -->
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6 grid-item cat-one cat-five cat-three" >
                        <div class="portfolio-wrapper">
                            <div class="portfolio-thumb portfolio-popup">
                                <img src="{!! asset('front/img/certificates/3.jpg') !!}" class="img-fluid" alt="">
                                <a href="{!! asset('front/img/certificates/3.jpg') !!}" class="view"><i class="icofont-plus"></i></a>
                            </div>
                           <!--  <div class="portfolio-content">
                                <h5><a href="#">Finance Consultancy</a></h5>
                                <span>supporting text</span>
                            </div> -->
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6 mb-30 grid-item cat-one cat-four cat-two">
                        <div class="portfolio-wrapper">
                            <div class="portfolio-thumb portfolio-popup">
                                <img src="{!! asset('front/img/certificates/4.jpg') !!}" class="img-fluid" alt="">
                                <a href="{!! asset('front/img/certificates/4.jpg') !!}" class="view"><i class="icofont-plus"></i></a>
                            </div>
                          <!--   <div class="portfolio-content">
                                <h5><a href="#">Finance Consultancy</a></h5>
                                <span>supporting text</span>
                            </div> -->
                        </div>
                    </div>
                      <div class="col-xl-3 col-lg-3 col-md-6 mb-30 mt-30 grid-item cat-five cat-two" style="top: 387px !important;">
                        <div class="portfolio-wrapper">
                            <div class="portfolio-thumb portfolio-popup">
                                <img src="{!! asset('front/img/certificates/5.jpg') !!}" class="img-fluid" alt="">
                                <a href="{!! asset('front/img/certificates/5.jpg') !!}" class="view"><i class="icofont-plus"></i></a>
                            </div>
                           <!--  <div class="portfolio-content">
                                <h5><a href="#">Finance Consultancy</a></h5>
                                <span>supporting text</span>
                            </div> -->
                        </div>
                    </div>
                     <div class="col-xl-3 col-lg-3 col-md-6 mb-30 grid-item cat-five cat-two">
                        <div class="portfolio-wrapper">
                            <div class="portfolio-thumb portfolio-popup">
                                <img src="{!! asset('front/img/certificates/6.jpg') !!}" class="img-fluid" alt="">
                                <a href="{!! asset('front/img/certificates/6.jpg') !!}" class="view"><i class="icofont-plus"></i></a>
                            </div>
                           <!--  <div class="portfolio-content">
                                <h5><a href="#">Finance Consultancy</a></h5>
                                <span>supporting text</span>
                            </div> -->
                        </div>
                    </div>

                     <div class="col-xl-3 col-lg-3 col-md-6 mb-30 grid-item cat-five cat-two">
                        <div class="portfolio-wrapper">
                            <div class="portfolio-thumb portfolio-popup">
                                <img src="{!! asset('front/img/certificates/7.jpg') !!}" class="img-fluid" alt="">
                                <a href="{!! asset('front/img/certificates/7.jpg') !!}" class="view"><i class="icofont-plus"></i></a>
                            </div>
                           <!--  <div class="portfolio-content">
                                <h5><a href="#">Finance Consultancy</a></h5>
                                <span>supporting text</span>
                            </div> -->
                        </div>
                    </div>

                     <div class="col-xl-3 col-lg-3 col-md-6 mb-30 grid-item cat-five cat-two">
                        <div class="portfolio-wrapper">
                            <div class="portfolio-thumb portfolio-popup">
                                <img src="{!! asset('front/img/certificates/8.jpg') !!}" class="img-fluid" alt="">
                                <a href="{!! asset('front/img/certificates/8.jpg') !!}" class="view"><i class="icofont-plus"></i></a>
                            </div>
                           <!--  <div class="portfolio-content">
                                <h5><a href="#">Finance Consultancy</a></h5>
                                <span>supporting text</span>
                            </div> -->
                        </div>
                    </div>

                     <div class="col-xl-3 col-lg-3 col-md-6 mb-30 grid-item cat-five cat-two">
                        <div class="portfolio-wrapper">
                            <div class="portfolio-thumb portfolio-popup">
                                <img src="{!! asset('front/img/certificates/9.jpg') !!}" class="img-fluid" alt="">
                                <a href="{!! asset('front/img/certificates/9s.jpg') !!}" class="view"><i class="icofont-plus"></i></a>
                            </div>
                           <!--  <div class="portfolio-content">
                                <h5><a href="#">Finance Consultancy</a></h5>
                                <span>supporting text</span>
                            </div> -->
                        </div>
                    </div>

                   <div class="col-xl-3 col-lg-3 col-md-6 mb-30 grid-item cat-five cat-two">
                    <div class="portfolio-wrapper">
                        <div class="portfolio-thumb portfolio-popup">
                            <img src="{!! asset('front/img/certificates/11.jpg') !!}" class="img-fluid" alt="">
                            <a href="{!! asset('front/img/certificates/10.jpg') !!}" class="view"><i class="icofont-plus"></i></a>
                        </div>
                       <!--  <div class="portfolio-content">
                            <h5><a href="#">Finance Consultancy</a></h5>
                            <span>supporting text</span>
                        </div> -->
                    </div>
                </div>

                </div>
            </div>
       </section>
<style>
 .portfolio-thumb.portfolio-popup {
    padding: 10px 0;
}.portfolio-wrapper { text-align: center; }
.portfolio-wrapper {
    border: 0 !important;
    box-shadow: 0px 0px 2px 1px #0000007d;
    border-radius: 0;
}.portfolio-thumb.portfolio-popup {
    padding: 10px;
}.portfolio-wrapper {
    border: 2px solid #343a40cc !important;
    box-shadow: 0px 0px 1px 1px #343a4061;
    border-radius: 0;
}
</style>
</main>
@endsection