@extends('master.default')
@section('content')

    <main>
        <!-- slider-area -->
        <section class="slider-area">
            <div class="slider-active">
                <div class="single-slider slider-bg d-flex align-items-center slide1">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="slider-content">
                                    <h1 data-animation="slideDown" data-delay=".2s">When the answer is<br><span>"NOT TONIGHT"</span><p>&nbsp;&nbsp;<span class="sm">Then you Need to Visit:</span></p></h1>
                                    <p data-animation="fadeInRight" data-delay=".4s"><span class="clinic">Nisha Health Care Clinic</span></p>
                                    <a href="{{url('contact')}}" class="btn" data-animation="fadeInUp" data-delay=".6s">get Appointment</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="single-slider slider-bg d-flex align-items-center slide2">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-12">
                                   <div class="slider-content">
                                    <h1 data-animation="fadeInUp" data-delay=".2s">Welcome to the world of<br><span>"SATISFACTION"</span></h1>
                                        <p>&nbsp;&nbsp;<span class="sm">Visit:</span></p>
                                    <p data-animation="fadeInRight" data-delay=".4s"><span class="clinic">Nisha Health Care Clinic</span></p>
                                    <a href="{{url('contact')}}" class="btn" data-animation="fadeInUp" data-delay=".6s">get Appointment</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
                <div class="single-slider slider-bg d-flex align-items-center slide3">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="slider-content">

                                    <h1 data-animation="slideDown" data-delay=".2s">Pregnancy Complications?</h1>
                                        <h3 class="sm-text">Don't Ignore</h3>
                                        <h4 class="sm-text">Meet:</h4>
                                        <h2 class="dr">Dr. Ruby Siddiqui</h2>
                                        <p data-animation="fadeInRight" data-delay=".4s">(Women's Infertility Specialist & Gynaecologist)</p>
                                        <a href="http://drmssiddiqui.com/contact" class="btn" data-animation="fadeInUp" data-delay=".6s">
                                        Consult Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </section>
        <!-- about-area -->
        <section class="about-area pb-20 pt-100">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 mb-40">
                        <div class="section-title">
                            <h2>About Dr. M.S. Siddiqui</h2>
                        </div>
                        <div class="about-content">
                            <p><span class="name">Dr. M.S Siddiqui</span> is one among the most eminent healthcare service provider in herbal space in Barabanki India. Established in the year 2009, it carries legacy of renowned unani scholars and physicians.<br>
                            <br>
                            <span class="sub-head">Mission</span><br>
                            At Noble our mission is to provide you with the best guidance available related to the most important aspect of your life - maintaining your excellent health. Along with this mission is our mandate to provide you with the herbal products you need, we always ensure that the consumer gets his value for money and this is the most prominent reason behind our successful establishment in domestic market and increasing impact in other countries.</p>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 mb-20 md-margin">
                        <div class="row">
                            <div class="col mb-20">
                                <div class="about-img">
                                    <img src="{!! asset('front/img/about1.jpg') !!}" class="img-fluid" alt="">
                                    <!-- <img src="{!! asset('front/img/about/about2.jpg') !!}" class="img-fluid" alt=""> -->
                                </div>
                            </div>
                            <div class="col mb-20">
                                <div class="about-img">
                                    <img src="{!! asset('front/img/about/profile.jpg') !!}" class="img-fluid" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- service-area -->
       <section class="service-area pt-50 pb-20 grey-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12 text-center pb-40 ">
                            <div class="section-title service-title">
                                <h2>Welcome to <span class="clinic">Nisha Health Care</span> Clinic</h2>
                                <p>We help you to get sexual consultation from Dr. M.S. Siddiqui (Nisha Health Care Clinic).</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 col-md-6 mb-50 ">
                            <div class="services-box">
                                <div class="services-img">
                                    <img src="{!! asset('front/img/service/1.jpg') !!}" class="img-fluid" alt="">
                                </div>
                                <div class="services-content">
                                    <div class="card-title">
                                        <h5>Sex Consultancy</h5>
                                    </div>
                                    <div class="card-text">
                                        <p>Dr. M.S. Siddiqui provides ultimate sex problem consultancy. He listens all the problems carefully and does the best diagnosis.</p>
                                    </div>
                                    <!-- <a href="javascript:;" class="btn">Details</a> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-6 mb-50 ">
                            <div class="services-box">
                                <div class="services-img">
                                    <img src="{!! asset('front/img/service/2.jpg') !!}" class="img-fluid" alt="">
                                </div>
                                <div class="services-content">
                                    <div class="card-title">
                                        <h5>Men Sex Problems</h5>
                                    </div>

                                    <div class="card-text">
                                        <p>All the sex problem in men like Erectile Dysfunction , Premature Ejeculation ,Noctural Emission , Infertility / Azospermia,Loss of Libido are diagnosed by Dr. M.S. Siddiqui. </p>
                                    </div>
                                  
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-6 mb-50 ">
                            <div class="services-box">
                                <div class="services-img">
                                    <img src="{!! asset('front/img/service/3.jpg') !!}" class="img-fluid" alt="">
                                </div>
                                <div class="services-content">
                                    <div class="card-title">
                                        <h5>Men Sex Treatment</h5>
                                    </div>
                                    <div class="card-text">
                                        <p>Unani system is based on the Hippocratic theory that a perfect balance of “Arkan” (elements), “Akhlata” (humors) and “Mizaj” (temperaments) helps the body healthy.</p>
                                    </div>
                                    <!-- <a href="javascript:;" class="btn">Details</a> -->
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
            </section>

            <section class="related-project pt-50 pb-20">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 text-center pb-40">
                        <div class="section-title service-title">
                            <h2>Diseases & Conditions</h2>
                            <p>Nisha Health Care is a clinic which specializes in curing sexual disorders from the root cause in both male and female using modern system of medicine.</p>
                        </div>
                    </div>
                </div>
               <div class="row service-shadow">
                   <div class="col-xl-4 col-lg-4 col-md-12 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="{{'male-sex-problems'}}">
                                    <img src="{!! asset('front/img/about/5.jpg') !!}" alt="">
                                </a>
                            </div>
                            <div class="link-box home-blog-link text-center">
                                <a href="{{'male-sex-problems'}}">Men's Sex Problems</a>
                            </div>
                        </div>
                    </div>
                     <div class="col-xl-4 col-lg-4 col-md-12 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="{{'female-sex-problems'}}">
                                      <img src="{!! asset('front/img/about/6.jpg') !!}" alt="">
                                </a>
                            </div>
                            <div class="link-box home-blog-link text-center">
                                <a href="{{'female-sex-problems'}}">Women's Sex Problems</a>
                            </div>
                        </div>
                    </div>
                     <div class="col-xl-4 col-lg-4 col-md-12 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="{{'male-factor-infertility'}}">
                                    <img src="{!! asset('front/img/about/7.jpg') !!}" alt="">
                                </a>
                            </div>
                            <div class="link-box home-blog-link text-center">
                                <a href="{{'male-factor-infertility'}}">Male Factor Infertility</a>
                            </div>
                        </div>
                    </div>
                     <div class="col-xl-4 col-lg-4 col-md-12 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="{{'female-factor-infertility'}}">
                                     <img src="{!! asset('front/img/about/8.jpg') !!}" alt="">
                                </a>
                            </div>
                            <div class="link-box home-blog-link text-center">
                                <a href="{{'female-factor-infertility'}}">Female Factor Infertility</a>
                            </div>
                        </div>
                    </div>
                     <div class="col-xl-4 col-lg-4 col-md-12 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="{{'common-diseases'}}">
                                     <img src="{!! asset('front/img/about/10.jpg') !!}" alt="">
                                </a>
                            </div>
                            <div class="link-box home-blog-link text-center">
                                <a href="{{'common-diseases'}}">Common Diseases</a>
                            </div>
                        </div>
                    </div>
                     <div class="col-xl-4 col-lg-4 col-md-12 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="{{'contact'}}">
                                    <img src="{!! asset('front/img/about/11.jpg') !!}" alt="">
                                </a>
                            </div>
                            <div class="link-box home-blog-link text-center">
                                <a href="{{'contact'}}">Online Consultation</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>

         <!-- portfolio area -->
        <section class="portfolio-area grey-bg pt-50 pb-20">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="col-xl-12 text-center pb-40 ">
                            <div class="section-title service-title">
                                <h2>Seminars & Events</h2>
                                <p>He provides Sex education, Seminars, counselling to enrich Peoples with communication, emotional bonding and art of love making. This empowers every individual and nurtures relationship..</p>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row portfolio-active">

                    @forelse($photos as $key => $photo)
                    <div class="col-xl-4 col-lg-4 col-md-6 mb-20 grid-item cat-four cat-two">
                        <div class="portfolio-wrapper">
                            <div class="portfolio-thumb portfolio-popup">
                                <img src="{!! asset('front/upload/gallary/'.$photo->image) !!}" class="img-fluid" alt="">
                                <a href="{!! asset('front/upload/gallary/'.$photo->image) !!}" class="view"><i class="icofont-plus"></i></a>
                            </div>
                            <div class="portfolio-content">
                                <h5><a href="javascript:;">Dr. M.S. Siddiqui</a></h5>
                                <span>{{$photo->title}}</span>
                            </div>
                        </div>
                    </div>
                    @empty
                    @endforelse
                
                </div>
            </div>
        </section>

         <!-- cta-area -->
        <section class="cta-area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 award-wrap text-center">
                        <div class="call-to-action white-text" style="padding-top: 20px;">
                            <h3>World Signature Award Winner <br>Honored By<br> Bollywood Actor Govinda </h3>
                            <div class="call-to-action">
                                <a href="https://www.youtube.com/embed/9-lrsnihO_Q" class="btn video-view btn-vd">watch video</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- testimonial-area -->
        <section class="testimonial-area pb-30 pt-100">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 text-center pb-10">
                        <div class="section-title service-title">
                            <h2>Patients Appreciation</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12">
                        <div class="testimonial-active owl-carousel">
                            <div class="single-item testimonial-items text-center">
                                <div class="client-text">
                                   Its been a wonderful experience for us while consulting Dr Siddiqui. Essentially we consulted him for lack of interest and inadequate performance in both of us. Dr Siddiqui led us on the path of emotional and sensual connect which has resulted in excellent sexual intimacy for both of us. Automatically our interest and performance has improved many fold. Our sex life has got magically transformed.
                                </div>
                                <h4>"Anil Shah"</h4>
                            </div>
                            <div class="single-item testimonial-items text-center">
                                <div class="client-text">
                               I have been experiencing vaginal laxity after my forcep delivery and lack of adequate suturing by my gyneac. This was not allowing adequate friction and pleasure whenever we made love. After Dr Siddiqui treatment and guidance it has really improved tone of my vaginal walls and my husband and I find our intimate moments much more pleasurable.
                                </div>
                                <h4>"Nilima Singh"</h4>
                            </div>
                            <div class="single-item testimonial-items text-center">
                                <div class="client-text">
                                    I was suffering with severe premature ejaculation since before my marriage. I could never satisfy my partner. I used to feel inadequate and guilty. After 9 years I decided to go for treatment. Fortunately my friend suggested name of Dr M.S Siddiqui. After four months of his treatment my timings have significantly improved and I can see my wife sexually satisfied and happy. I wish I had come early, to solve my problem, to consult Dr Siddiqui.
                                </div>
                                <h4>"Nilesh Mittal"</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- team-are -->
       
        <!-- clients-area -->
         <div class="clients-area pt-30 pb-50 grey-bg">
            <div class="container">
                <div class="row">
                    <div class="clients-active owl-carousel">
                        <div class="col-sm-12">
                            <div class="single-clients">
                                <img src="{!! asset('front/img/clients/global.png') !!}" alt="">
                                <p>Globally Recognized</p>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="single-clients">
                                <img src="{!! asset('front/img/clients/ayurvedic.png') !!}" alt="">
                                <p>Ayurvedic Treatments</p>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="single-clients">
                                <img src="{!! asset('front/img/clients/certified.png') !!}" alt="">
                                <p>Certified</p>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="single-clients">
                                <img src="{!! asset('front/img/clients/trust.png') !!}" alt="">
                                <p>100% Privacy</p>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="single-clients">
                                <img src="{!! asset('front/img/clients/live.png') !!}" alt="">
                                <p>Live Chat</p>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="single-clients">
                                <img src="{!! asset('front/img/clients/solution.png') !!}" alt="">
                                <p>Sex Consultacy</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <script type="text/javascript">
        $(document).ready(function(){
            $(".btn-vd").click(function(){
                $(".mfp-iframe").attr('src', 'https://www.youtube.com/embed/9-lrsnihO_Q');
            });
        });
    </script>
  
@endsection

  