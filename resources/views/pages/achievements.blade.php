@extends('master.default')
@section('content')

<main>
    <!-- breadcrumb-area -->
    <section class="breadcrumb-area blue-bg">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-6 mb-15 mt-15">
                    <div class="breadcrumb-title">
                        <h3>Achievments</h3>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Gallary</a></li>
                            <li><a href="#">Achievments</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Gallary -->
    <section class="portfolio-area grey-bg pt-70 pb-50">
        <div class="container">       
            <div class="row portfolio-active">
                <div class="col-xl-4 col-lg-4 col-md-6 mb-30 grid-item cat-four cat-two">
                    <div class="portfolio-wrapper">
                        <div class="portfolio-thumb portfolio-popup">
                            <img src="img/portfolio/p1.jpg" class="img-fluid" alt="">
                            <a href="img/portfolio/p1.jpg" class="view"><i class="icofont-plus"></i></a>
                        </div>
                        <div class="portfolio-content">
                            <h5><a href="#">Finance Consultancy</a></h5>
                            <span>supporting text</span>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 mb-30 grid-item cat-one cat-three">
                    <div class="portfolio-wrapper">
                        <div class="portfolio-thumb portfolio-popup">
                            <img src="img/portfolio/p2.jpg" class="img-fluid" alt="">
                            <a href="img/portfolio/p2.jpg" class="view"><i class="icofont-plus"></i></a>
                        </div>
                        <div class="portfolio-content">
                            <h5><a href="#">Finance Consultancy</a></h5>
                            <span>supporting text</span>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 mb-30 grid-item cat-one cat-five cat-three">
                    <div class="portfolio-wrapper">
                        <div class="portfolio-thumb portfolio-popup">
                            <img src="img/portfolio/p3.jpg" class="img-fluid" alt="">
                            <a href="img/portfolio/p3.jpg" class="view"><i class="icofont-plus"></i></a>
                        </div>
                        <div class="portfolio-content">
                            <h5><a href="#">Finance Consultancy</a></h5>
                            <span>supporting text</span>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 mb-30 grid-item cat-one cat-four cat-two">
                    <div class="portfolio-wrapper">
                        <div class="portfolio-thumb portfolio-popup">
                            <img src="img/portfolio/p4.jpg" class="img-fluid" alt="">
                            <a href="img/portfolio/p4.jpg" class="view"><i class="icofont-plus"></i></a>
                        </div>
                        <div class="portfolio-content">
                            <h5><a href="#">Finance Consultancy</a></h5>
                            <span>supporting text</span>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 mb-30 grid-item cat-four cat-two">
                    <div class="portfolio-wrapper">
                        <div class="portfolio-thumb portfolio-popup">
                            <img src="img/portfolio/p5.jpg" class="img-fluid" alt="">
                            <a href="img/portfolio/p5.jpg" class="view"><i class="icofont-plus"></i></a>
                        </div>
                        <div class="portfolio-content">
                            <h5><a href="#">Finance Consultancy</a></h5>
                            <span>supporting text</span>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 mb-30 grid-item cat-one cat-three">
                    <div class="portfolio-wrapper">
                        <div class="portfolio-thumb portfolio-popup">
                            <img src="img/portfolio/p6.jpg" class="img-fluid" alt="">
                            <a href="img/portfolio/p6.jpg" class="view"><i class="icofont-plus"></i></a>
                        </div>
                        <div class="portfolio-content">
                            <h5><a href="#">Finance Consultancy</a></h5>
                            <span>supporting text</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection