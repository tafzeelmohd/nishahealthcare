@extends('master.default')

@section('content')



<main>

    <!-- breadcrumb-area -->

    <section class="breadcrumb-area blue-bg">

        <div class="container">

            <div class="row">

                <div class="col-xl-6 col-md-6 mb-15 mt-15">

                    <div class="breadcrumb-title">

                        <h3>Low Testosterone & Androgens</h3>

                    </div>

                </div>

                <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">

                    <div class="breadcrumb">

                        <ul>

                            <li><a href="#">Home</a></li>

                            <li><a href="#">Men's Sex Problems</a></li>

                            <li><a href="#">Low Testosterone & Androgens</a></li>

                        </ul>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <!--Premeture Ejeculation Problems -->



    <div class="services-area pt-50 pb-65">

    <div class="container">

        <div class="row">

            <div class="col-xl-8 col-lg-8">

                <div class="row">

                    <div class="col-xl-12">

                        <div class="service-details mb-40">

                            <img class="mb-30" src="{!! asset('front/img/mens-problems/low-testos-details.jpg') !!}" alt="">

                            <h3>Low Testosterone & Androgens ?</h3>

                            <div class="mid-bar"></div>

                            <p>Testosterone is a hormone. It’s made by the bodies of both men and women. It plays a role in puberty and fertility. It also affects sexual desire. In men, most testosterone is made in the testes. In women, most testosterone is made in the ovaries.



                            <p>Men have higher levels of testosterone than women. It’s thought that testosterone greatly influences the development of many characteristics considered to be male traits. It helps increase muscle bulk, bone mass, physical strength, and the amount of body hair.</p>



                            <p>The levels of testosterone in your body are constantly changing in response to your body’s needs. However, the overall level of testosterone in your body changes throughout your lifetime as well. It usually decreases, particularly for men aged 30 years and older. For some men, these levels can become too low and cause unwanted effects that have them looking for ways to increase their testosterone levels.</p></p>



                            <p>Symptoms of abnormally low testosterone can be bothersome. Some symptoms can have an impact on your quality of life. These symptoms can include:</p>

                            <br>



                            <div class="row">

                                <div class="col-xl-6 col-lg-12">

                                    <div class="s-details-img mb-30">

                                        <img src="{!! asset('front/img/mens-problems/low-testosterone-effects.jpg') !!}" alt="">

                                    </div>

                                </div>

                             </div>



                             <p>If you’re experiencing one or more of these symptoms and don’t believe they’re caused by something else, talk to your doctor. If your doctor thinks your symptoms are related to abnormally low testosterone levels, they can test your levels. </p>

                            

                        </div>

                    </div>

                </div>

            </div>




                 <div class="col-xl-4 col-lg-4">
                    @include('pages/latest-news')
                    @include('pages/related-diseas')
                    @include('pages/quick-contact')
                </div>


        </div>

    </div>

    </div>



</main>

@endsection