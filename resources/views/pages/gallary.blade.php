@extends('master.default')
@section('content')

<main>
    <!-- breadcrumb-area -->
    <section class="breadcrumb-area blue-bg">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-6 mb-15 mt-15">
                    <div class="breadcrumb-title">
                        <h3>Gallary</h3>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">
                    <div class="breadcrumb">
                       
                       
                        <ul>
                            <li><strong><a href="{{route('uploadPhoto')}}">Upload Photos</a></strong></li>
                        </ul>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Painfull Intercourse Problems -->

       <section class="portfolio-area grey-bg pt-50 pb-40">
            <div class="container">
                <div class="row portfolio-active">

                    @forelse($photos as $key => $photo)
                    <div class="col-xl-4 col-lg-4 col-md-6 mb-20 grid-item">
                        <div class="portfolio-wrapper">
                            <div class="portfolio-thumb portfolio-popup">
                                <img src="{!! asset('front/upload/gallary/'.$photo->image) !!}" class="img-fluid" alt="">
                                 <a href="{!! asset('front/upload/gallary/'.$photo->image) !!}" class="view"><i class="icofont-plus"></i></a>
                            </div>
                        </div>
                    </div>
                    @empty
                    @endforelse


                </div>
                
                  {{$photos->links()}}
            </div>
       </section>
</main>
@endsection

<style type="text/css">
    ul.pagination li {
    float: left;
}.page-item.active .page-link {
    z-index: 1;
    color: #fff;
    background-color: #be1c2b !important;
    border-color: #be1c2b !important;
}.page-link { color: #000; }
</style>