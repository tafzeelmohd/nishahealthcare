@extends('master.default')

@section('content')



<main>

    <!-- breadcrumb-area -->

    <section class="breadcrumb-area blue-bg">

        <div class="container">

            <div class="row">

                <div class="col-xl-6 col-md-6 mb-15 mt-15">

                    <div class="breadcrumb-title">

                        <h3>Piles / Fistula</h3>

                    </div>

                </div>

                <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">

                    <div class="breadcrumb">

                        <ul>

                            <li><a href="#">Home</a></li>

                            <li><a href="#">Common Diseases</a></li>

                            <li><a href="#">Piles-Fistula</a></li>

                        </ul>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <!--Premeture Ejeculation Problems -->



    <div class="services-area pt-50 pb-65">

    <div class="container">

        <div class="row">

            <div class="col-xl-8 col-lg-8">

                <div class="row">

                    <div class="col-xl-12">

                        <div class="service-details mb-20">

                            <img class="mb-30" src="{!! asset('front/img/common-dis/piles.jpg') !!}" alt="">

                            <br>

                            <h3>Piles / Fistula ?</h3>

                            <div class="mid-bar"></div>

                            <p>In medicine, a fistula (pl. fistulas or fistulae) is an abnormal[1] connection or passageway between two epithelium-lined organs or vessels that normally do not connect. It is generally a disease condition, but a fistula may be surgically created for therapeutic reasons.</p>



                            <p>Fistulas are an abnormal small opening next to the anus from where discharge keeps occurring. This is due to a tunnel like tract between the anal canal and the skin. This condition always requires surgery for cure.</p>



                            <p>A fistula (in Ano - which I suppose is what you mean) is an abnormal passage/tract that has been formed from the inside of the rectum (mainly anal crypts) to the skin on the outside, following an abscess formation in this area. This is an abnormal type of healing. There is constant discharge through this as well as ocassionally inflammation and tnederness.Piles and Haemorrhoids are generally the same thing. But there is something called an"external pile" or a "sentinel pile", which is something that forms following fissures in the anus, due to hard stools. These may also bleed form time to time. The bleeding is basically capillary in origin.</p>



                            <p>Haemorrhoids are internal varicosities of small verins that are at the junction of venous plexues. They are internal piles as well as external piles - 2 different plexuses. The basic fault is like a varicosity, due to increased pressure in the area from prolonged sitting, excessive straining at stool, obesity, pregnancy etc. The result is engorgement of the veins, and bleeding through the stretched thin vessel walls.</p>



                            <p>In acute conditions of all three problems, many homeopathic remedies may be indicated depending on the symptoms. But in chronic cases, it is only the Consitutional Similimum taken with the whole history, that is helpful.</p>

                                

                            </div>

                        </div>

                    </div>

                </div>

          




                 <div class="col-xl-4 col-lg-4">
                    @include('pages/latest-news')
                    @include('pages/related-diseas')
                    @include('pages/quick-contact')
                </div>


        </div>

    </div>

    </div>



</main>

@endsection