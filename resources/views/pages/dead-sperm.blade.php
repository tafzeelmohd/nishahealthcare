@extends('master.default')

@section('content')



<main>

    <!-- breadcrumb-area -->

    <section class="breadcrumb-area blue-bg">

        <div class="container">

            <div class="row">

                <div class="col-xl-6 col-md-6 mb-15 mt-15">

                    <div class="breadcrumb-title">

                        <h3>Dead Sperm</h3>

                    </div>

                </div>

                <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">

                    <div class="breadcrumb">

                        <ul>

                            <li><a href="#">Home</a></li>

                            <li><a href="#">Male Factor Infertility</a></li>

                            <li><a href="#">Dead Sperm</a></li>

                        </ul>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <!--Premeture Ejeculation Problems -->



    <div class="services-area pt-50 pb-65">

    <div class="container">

        <div class="row">

            <div class="col-xl-8 col-lg-8">

                <div class="row">

                    <div class="col-xl-12">

                        <div class="service-details mb-20">

                            <img class="mb-30" src="{!! asset('front/img/male-factor-infertility/dead-sperm.jpg') !!}" alt="">

                            <br>

                            <h3>Dead Sperm ?</h3>

                            <div class="mid-bar"></div>

                            <p><strong>What is Dead Sperms:</strong> When semen has less of mature normal sperms & more of dead sperms this condition is abnormal. When ever there is less of normal sperm then chances of spontaneous pregnancy decreases (i.e. difficulty in conceiving i.e. wife does not becomes pregnant). This is one of the common causes of male factor infertility. This is also one of the most common semen abnormalities in men.</p>

                            <p><strong>How sperms develop:</strong> When boy becomes of 14 years of age then L.H. & F.S.H. hormone secretion from pituitary increases. The rise in these hormones leads to proliferation of sperm forming cells (Germ Cells) in the testis. These germ cells start multiplying under the effect of above-mentioned pituitary  hormones along with assistance of other hormones as testosterones, Growth hormones, Androstenidione, insulin like growth factor-I, Thyroids hormone, paracrine hormone & growth factors. Under the control of above-mentioned hormones germs cells divide & transformed into primary spermatocytes. Then further maturation of primary spermatocytes to spermatids & then finally into mature spermatozoa (i.e. normal sperms) occurs under the control of above-mentioned hormones. After few weeks of progressive maturation inside the testis these sperms become normally motile & develop the capacity to fertilize the ovum. This total sperm cycle from first stage to final stage of normal mature sperms is of three months. Any hindrance in the development of these spermatozoa will lead to dead sperms, less count of sperm & decreased motility, immotile or even dead sperms</p>

                         

                            <p><strong>Causes of dead sperm: The various causes of dead sperms are as follows:</strong></p>



                            <h3>1) Deficiency of central sperm producing hormones: </h3>

                              <div class="mid-bar"></div>

                                <div class="service-details mt-10 mb-40">

                                <p>Hypothalamic – pituitary deficiency: Idiopathic GnRH deficiency, Kallman syndrome, Prader-Willi syndrome, Laurence-Moon-Biedl syndrome, Hypothalamic deficiency, pituitary hypoplasia, Trauma, post surgical, postiradiation, Tumour (Adenoma, craniopharyngioma, other), Vascular (pituitary infraction, carotid aneurysm), Infiltrative (Sarcoidosis, histiocytosis, hemochromatosis) Autoimmune hypophysitis, Drugs (drug-induced hyperprolactinemia,  steroids use).</p>

                                <p>Untreated endocrinopathies, Glucocorticoid excess, Hypopituitarism, Isolated gonadotropin deficiency (non acquired): Pituitary, Hypothalamic, Associated with multiple pituitary hormone deficiencies: Idiopathic pan hypo pituitarism (hypothalamic defects), Pituitary dysgenesis, Space-occupying lesions(craniopharyngioma, Rathke pouch cysts, hypothalamic tumors, pituitary adenomas), , Laurence-Moon-Beidl syndrome Prader-Willi syndrome , Frohlich syndrome, Hypergonadotropic hypogonadism : Klinefelter syndrome,  Noonan syndrome,  Viral orchitis, Cytotxic drugs, Testicular irradiation. </p>



                                <h3><strong>Testicular disorders: </strong></h3>

                                <div class="mid-bar"></div>

                                <p> Testicular disorders (primary leydig cell dysfunction i.e. Hypoganadism), Chromosomal (Klinefelter syndrome and variants, XX male gonadal dysgenesis), Defects in androgen biosynthesis, Orchitis (mumps, HIV, other viral, ),Myotonia dystrophica, Toxins (alcohol, opiates, fungicides, insecticides, heavy metals, cotton seed oil), Drugs (cytotoxic drugs, ketoconazole, cimetidine, spironolactone)</p>



                                <p><strong> 3) Varicocele:</strong>  varicocele is dilatation of scrotal vein in the scrotum that leads to rise in temperature of testis and raise testicular temperature, resulting in less sperm production & death of whatever sperms are produced. </p>



                                <p><strong>4) Drugs </strong>(e.g. spironolactone, ketoconazole, cyclophosphamide, estrogen administration, sulfasalazine)</p>



                                <p><strong>5) Autoimmunity i.e. presence of Antisperm antibody.</strong> These Antisperm antibodies bind with sperms & either make them less motile, totally immotile or even dead which is called necrospermia.</p>

                                <p><strong>6) Undescended testicle (cryptorchidism).</strong> Undescended testis is a condition when one or both testicles fail to descend from the abdomen into the lower part of scrotum during fetal development. Undescended testicles can lead to less sperm production. Because the testicles temperature increase due to the higher internal body temperature compared to the temperature in the scrotum, sperm production may be affected.</p>

                                <p><strong>7)</strong> Mosaic Klinefelter's syndrome.</strong> In this disorder of the  chromosomes, of the man is abnormal. This causes abnormal development of the testicles, resulting in low sperm production. Testosterone production may be low or normal.</p>

                                <p><strong>8)</strong> Viral Orchits</strong> as mumps or other viral infections. </p>

                                <p><strong>9)</strong> Infections as tuberculosis, sarcoidosis involving testis or surrounding structures as epididymis. 

                                <p><strong>10)</strong> Chronic systemic diseases as Liver diseases, Renal failure, Sickle cell disease, Celiac disease  

                                <p><strong>11)</strong> Neurological disease as myotonic dystrophy  </p>

                                <p><strong>12)</strong> Development and structural defects as mild degree of Germinal cell hypo-plasia</p>

                                <p><strong>13)</strong> Partial Androgen resistance  </p>

                                <p><strong>14)</strong> Mycoplasmal infection  </p>

                                <p><strong>15)</strong> Partial Immotile cilia syndrome </p>

                                <p><strong>16)</strong> Partial Spermatogenic arrest due to interruption of the complex process of germ cell  differentiation from spermatid level to the formation of mature spermatozoa results in decreased sperm count i.e. oligospermia. Its diagnosis is made by testicular biopsy. This is found in upto 30% of all cases of dead sperm patients.</p>

                                <p><strong>17)</strong> Heat Exposure to testis: as febrile illness or exposure to hot ambience induces a abnormality in spermatogenesis.</p>

                                <p><strong>19)</strong> Infection – as bacterial epididimo-orchitis, even in prostatis spermatogenic defect have been noted.

                                <p><strong>20)</strong> Hyper-thermia due to cryptorchidism</p>

                                <p><strong>21)</strong> Chromosomal abnormality: has been found in many cases of low sperm count</p>

                                <p><strong>22)</strong> Alcohol use, Cocaine or heavy marijuana use or Tobacco smoking may lower sperm count</p>

                                <p><strong>23)</strong> Anti-sperm antibodies.  In some people there occurs development of some abnormal blood proteins called anti-sperm antibodies, which binds with sperm and make them either immotile or dead or decrease their count.</p>

                                <p><strong>24)</strong> Infections. Infection of uro-genital tract may affect sperm production. Repeated bouts of infections are one of the common causes associated with male infertility.</p>

                                <p><strong>25)</strong> Klinefelter's syndrome. In this disorder of the  chromosomes, a man has two X chromosomes and one Y chromosome instead of one X and one Y. This causes abnormal development of the testicles, resulting in low or absent sperm production. Testosterone production also may be lower.</p>

                                <p><strong>26)</strong> Trauma to testis</p>

                                <p><strong>27)</strong> Environmental toxins: as Pesticides and other chemicals in food  or as ayurvedic medicines.</p>

                                <p><strong>28)</strong> Genetic Factors: as idiopathic partial hypo-gonadotropic hypogonadism

                                Diagnosis of Cause of Dead Sperms</p>

                                <p>For correct diagnosis of cause of more of Dead sperm, we need detail history & physical examinations then certain relevant investigations are required.</p>

                                

                               </div>

                        </div>

                    </div>

                </div>

            </div>



            <div class="col-xl-4 col-lg-4">
                    @include('pages/latest-news')
                    @include('pages/related-diseas')
                    @include('pages/quick-contact')
                </div>

        </div>

    </div>

    </div>



</main>

@endsection