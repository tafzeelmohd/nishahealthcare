@extends('master.default')
@section('content')

<main>
    <!-- breadcrumb-area -->
    <section class="breadcrumb-area blue-bg">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-6 mb-15 mt-15">
                    <div class="breadcrumb-title">
                        <h3>Male Factor Infertility</h3>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Service</a></li>
                            <li><a href="#">Male Factor Infertility</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Male Sexual Problems -->

        <section class="related-project pt-50 pb-70">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 text-center pb-40">
                        <div class="section-title service-title">
                            <h2>Male Factor Infertility</h2>
                            <p>Infertility is often believed to be a woman's problem. However, studies indicate that 30% of infertility is related to male factor problems such as structural abnormalities, sperm production disorders, ejaculatory disturbances and immunologic disorders..</p>
                        </div>
                    </div>
                </div>
               <div class="row service-shadow">
                   <div class="col-xl-4 col-lg-4 col-md-12 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="{{'low-sperm-count'}}">
                                    <img src="{!! asset('front/img/about/5.jpg') !!}" alt="">
                                </a>
                            </div>
                            <div class="link-box home-blog-link text-center">
                                <a href="{{'low-sperm-count'}}">Low Sperm Count</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-12 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="{{'nil-sperm-count'}}">
                                      <img src="{!! asset('front/img/about/7.jpg') !!}" alt="">
                                </a>
                            </div>
                            <div class="link-box home-blog-link text-center">
                                <a href="{{'nil-sperm-count'}}">Nil Sperm</a>
                            </div>
                        </div>
                    </div>
                     <div class="col-xl-4 col-lg-4 col-md-12 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="{{'dead-sperm'}}">
                                     <img src="{!! asset('front/img/about/10.jpg') !!}" alt="">
                                </a>
                            </div>
                            <div class="link-box home-blog-link text-center">
                                <a href="{{'dead-sperm'}}">Dead Sperm</a>
                            </div>
                        </div>
                    </div>
                    
                 </div>
            </div>
        </section>
</main>
@endsection