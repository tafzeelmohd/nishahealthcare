@extends('master.default')

@section('content')



<main>

    <!-- breadcrumb-area -->

    <section class="breadcrumb-area blue-bg">

        <div class="container">

            <div class="row">

                <div class="col-xl-6 col-md-6 mb-15 mt-15">

                    <div class="breadcrumb-title">

                        <h3>Diabetes</h3>

                    </div>

                </div>

                <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">

                    <div class="breadcrumb">

                        <ul>

                            <li><a href="#">Home</a></li>

                            <li><a href="#">Common Diseases</a></li>

                            <li><a href="#">Diabetes</a></li>

                        </ul>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <!--Premeture Ejeculation Problems -->



    <div class="services-area pt-50 pb-65">

    <div class="container">

        <div class="row">

            <div class="col-xl-8 col-lg-8">

                <div class="row">

                    <div class="col-xl-12">

                        <div class="service-details mb-20">

                            <img class="mb-30" src="{!! asset('front/img/common-problems/diabetes-detils.jpg') !!}" alt="">

                            <br>

                            <h3>Diabetes ?</h3>

                            <div class="mid-bar"></div>

                            <p>The diabetes nearly always presents with the symptoms of metabolic disturbance, weakness, fatigue, loss of weight, thirst, cramps frequency of micturition, and visual impairment. In some cases, abdominal pain is the presenting symptom and has to be distinguished from the pain of an acute abdomen. In the only patient I have seen presenting in this way, the diagnosis was made clear by the strong smell of ketones. Testing the urine for sugar and ketones is very sensitive and enables the diagnosis to be confirmed on the spot. Ketonuria as the time of presentation, however is less common than it used to be. A decision must them be made about the initial management. If ketoacidosis is present, immediate admission to hospital is required. Disasters have occurred because out patient blood tests have been ordered, thus delaying admission or because admission has been postponed to the following day. Vomiting is an especially serious symptom in ketoacidosis. Wherever or not patients with type I diabetes without ketoacidosis are admitted to hospital will depend on a number of factors, among them severity of disease, age of patient, family factors and experience of the physician.</p>



                            <p>Type II diabetes may also present with loss of weight, thirst and fatigue. Frequently, however, the presenting symptoms are those of the complications of diabetes. The following are all common modes of presentation: Puritus vulvae, vulvitis, or vulval eczema. Skin sepsis boils, carbuncles, whitlows, cellulitis, and infected eczema.</p>



                            <p>Leg ulcers: - Pain and paraethesiae in the limbs symptoms or peripheral neuropathy. Balanitis the importance of balanitis as a presenting symptoms is not widely appreciated, although is mentioned by Osler. Urinary symptom diabetes may present with a urinary infection or with frequency of micturition caused by polyuria. In the elderly, the sudden onset of nocturnal incontinence should suggest diabetes.</p>



                            <p>Elderly patients sometimes present with drowsiness, confusion or coma and dehydration because of hyperglycemic nonketotic coma or precoma. The condition resembles a cerebrovascular accident, and the diagnosis may be missed if a blood sugar is not doming routinely in-patients suspected of having cereprovascular accidents.</p>



                            <p>Gestational diabetes is the one form that should be detected by screening all pregnant women. At least for glycosuria should be done at all prenatal visits, and those testing positive should have fasting and post pandaial blood sugars, women who are at high risk for gestational diabetes should have blood sugars done early in pregnancy, whether or not they have glysosuria. The risk factors are obesity, advanced age, parity of five or more, previous delivery of over weight baby, history of still birth or spontaneous abortion, fetal malformation in previous pregnancy and diabetes in a first degree relative.</p>

                            

                            <p>The assessment of a patient with diabetes is a good example of the need for a system approach. A well adjusted patient with a supportive family and few environmental stresses is likely to find it relatively easy to attain goals of therapy and maintain control. Patients without these advantages may present several kinds of management problems. If there are family problems, the patients may be too preoccupied by them to attend to his or her own needs, lack of family support may make it difficult to adhere to a diet.</p>

                            

                            <p>The diabetes itself may be used for secondary gain by the patient, thus giving him or her and interest in its continuation. Emotional stresses can have a direct effect on carbohydrate and fat metabolism. Control may also be compromised by self-destructive behavior, such as excessive alcohol and food consumption or deliberate omission of insulin doses.</p>



                            <p><strong>What do we do about it?</strong></p>

                            <p>Age-old unani system acknowledged diabetes thousands of years ago and evolved effective & permanent cure for diabetics traditional Hakeems with their vast knowledge and experience evolved potent medicine for a complete relief from diabetes.

                            </p>





                            </div>

                        </div>

                    </div>

                </div>

          



            <div class="col-xl-4 col-lg-4">
                    @include('pages/latest-news')
                    @include('pages/related-diseas')
                    @include('pages/quick-contact')
                </div>
        </div>

    </div>

    </div>



</main>

@endsection