@extends('master.default')

@section('content')



<main>

    <!-- breadcrumb-area -->

    <section class="breadcrumb-area blue-bg">

        <div class="container">

            <div class="row">

                <div class="col-xl-6 col-md-6 mb-15 mt-15">

                    <div class="breadcrumb-title">

                        <h3>Low Sperm Count</h3>

                    </div>

                </div>

                <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">

                    <div class="breadcrumb">

                        <ul>

                            <li><a href="#">Home</a></li>

                            <li><a href="#">Male Factor Infertility</a></li>

                            <li><a href="#">Low Sperm Count</a></li>

                        </ul>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <!--Premeture Ejeculation Problems -->



    <div class="services-area pt-50 pb-65">

    <div class="container">

        <div class="row">

            <div class="col-xl-8 col-lg-8">

                <div class="row">

                    <div class="col-xl-12">

                        <div class="service-details mb-20">

                            <img class="mb-30" src="{!! asset('front/img/male-factor-infertility/low-sperm.jpg') !!}" alt="">

                            <br>

                            <h3>Low Sperm Count ?</h3>

                            <div class="mid-bar"></div>

                            <p><strong>What is Low sperm count:</strong> When sperm count is less i.e. decreased in number or amount than normal is called oligospermia i.e. low sperm count. Whenever there is less sperm count then chances of spontaneous pregnancy decreases (i.e. difficulty in conceiving i.e. wife does not becomes pregnant). This is one of the common causes of male factor infertility. This is also one of the most common semen abnormalities in men.

                            To conceive a child, a male's sperm must combine with a female's egg. The testicles make and store sperm, which are ejaculated by the penis to deliver sperm to the female reproductive tract during sexual intercourse.

                            The most common issues that lead to infertility in men are problems that affect how the testicles work. Other problems are hormone imbalances or blockages in the male reproductive organs. In about 50% of cases, the cause of male infertility cannot be determined.|

                            A complete lack of sperm is the cause of infertility in about 10% to 15% of men who are infertile. When a man does not produce sperm, it is called azoospermia . A hormone imbalance or a blockage of sperm movement can cause azoospermia.

                            In some cases of infertility, a man produces less sperm than normal. This condition is called oligospermia. The most common cause of oligospermia is varicocele, an enlarged vein in the testicle.</p>

                           

                            <br>

                            <h3>Oligospermia:</h3>

                              <div class="mid-bar"></div>

                                <div class="service-details mt-10 mb-40">

                                <p>Oligospermia or Oligozoospermia, refers to semen with a low concentration of sperm and is a common finding in male infertility. Often semen with a decreased sperm concentration may also show significant abnormalities in sperm morphology and motility. There has been interest in replacing the descriptive terms used in semen analysis with more quantitative information.</p>



                                <p>The diagnosis of oligozoospermia is based on one low count in a semen analysis performed on two occasions. For many decades sperm concentrations of less than 20 million sperm/ml were considered low or oligospermic, recently, however, the WHO reassessed sperm criteria and established a lower reference point, less than 15 million sperm/ml, consistent with the 5th percentile for fertile men. Sperm concentrations fluctuate and oligospermia may be temporary or permanent.</p>



                                <p>Sources usually classify oligospermia in 3 classes</p>

                                <p>Mild: concentrations 10 million – 20 million sperm/ml</p>

                                <p>Moderate: concentrations 5 million – 10 million sperm/ml</p>

                                <p>Severe: concentrations less than 5 million sperm/ml</p>



                                <h6><strong>Causes:</strong></h6>

                                <p>Woman’s desire for sex is based on a complex interaction of many components affecting intimacy, including physical well-being, emotional well-being, experiences, beliefs, lifestyle and current relationship. If you're experiencing a problem in any of these areas, it can affect your sexual desire.</p>

                                <br>



                                <h3><strong>How sperms develop: </strong></h3>

                                <div class="mid-bar"></div>

                                <p>When boy becomes of 14 years of age then L.H. & F.S.H. hormone secretion from pituitary increases. The rise in these hormones leads to proliferation of sperm forming cells (Germ Cells) in the testis. These germ cells start multiplying under the effect of above-mentioned pituitary sex hormones along with assistance of other hormones as testosterones, Growth hormones, Androstenidione, insulin like growth factor-I, Thyroids hormone, paracrine hormone & growth factors. Under the control of above-mentioned hormones germs cells divide & transformed into primary spermatocytes. Then further maturation of primary spermatocytes to spermatids & then finally into mature spermatozoa (i.e. normal sperms) occurs under the control of above-mentioned hormones. After few weeks of progressive maturation inside the testis these sperms become normally motile & develop the capacity to fertilize the ovum. This total sperm cycle from first stage to final stage of normal mature sperms is of three months. Any hindrance in the development of these spermatozoa will lead to less count of sperm & decreased motility, immotile or even dead sperms.</p>



                                <p><strong>Causes of low sperm count:</strong> The various causes of low sperm count are as follows:</p>



                                <p>a) Deficiency of central sperm producing hormones as LH, FSH & other hormones may occur due to any of the below mentioned cause as Hypothalamic – pituitary deficiency: Idiopathic GnRH deficiency, Kallman syndrome, Prader-Willi syndrome, Laurence-Moon-Biedl syndrome, Hypothalamic deficiency, pituitary hypoplasia, Trauma, post surgical, postiradiation, Tumour (Adenoma, craniopharyngioma, other), Vascular (pituitary infraction, carotid aneurysm), Infiltrative (Sarcoidosis, histiocytosis, hemochromatosis) Autoimmune hypophysitis, Hypopituitarism, Pituitary, Hypothalamic, Associated with multiple pituitary hormone deficiencies: Idiopathic pan hypo pituitarism (hypothalamic defects), Pituitary dysgenesis, Space-occupying lesions (craniopharyngioma, Rathke pouch cysts, hypothalamic tumors, pituitary adenomas), , Laurence-Moon-Beidl syndrome Prader-Willi syndrome , Frohlich syndrome</p>

                                <p>b) Drugs (drug-induced hyperprolactinemia, sex steroids use), Glucocorticoid excess</p>

                                <p>c) Isolated gonadotropin deficiency (non acquired)</p>

                                <p>bstruction in outflow tract from testis to penile pening in epididymis or of vas deferens (cystic fibrosis, diethlstibesterol exposure) also called obstructive oligospermia.</p> 

                                 <p>4) Varicocele:  varicocele is dilatation of scrotal vein in the scrotum that leads to rise in temperature of testis and raise testicular temperature, resulting in less sperm production & death of whatever sperms are produced.</p>

                                 <p>5) Partial Spermatogenic arrest due to interruption of the complex process of germ cell  diffrentation from spermatid level to the formation of mature spermatozoa results in decreased sperm count i.e. oligospermia. Its diagnosis is made by testicular biopsy. This is found in upto 30% of all cases of low sperm count patients.18) Heat Exposure to testis: as febrile illness or exposure to hot ambience induces a fall in sperm count which is usually reversible. </p>

                                 <p>6) Autoimmunity i.e. presence of Antisperm antibody. In some people there occurs development of some abnormal blood proteins called anti-sperm antibodies which binds with sperm and make them either immotile or dead or decrease their count. These Antisperm antibodies bind with spems & either make them less motile, totally imotile or even dead which is called necrospermia.  </p>

                                 <p>7) Undescended testicle (cryptorchidism). Undescended testis is a condition when one or both testicles fail to descend from the abdomen into the lower part of scrotum during fetal development. Undescended testicles can lead to less sperm production. Because the testicles temperature increase due to the higher internal body temperature compared to the temperature in the scrotum, sperm production may be affected.</p>

                                 <p>8) Infections. Infection of urogenital tract may affect sperm production. Repeated bouts of infections are one of the common causes associated with male infertility.</p>

                                 <p>9) Viral orchits as mumps or other viral infections. </p> 

                                 <p>10) Infections as tuberculosis, sarcoidosis involving testis or surrounding structures as epididymis. </p>

                                 <p>11) Chronic systemic diseases as Liver diseases, Renal failure, Sickle cell disease, Celiac disease  </p>

                                 <p>12) Neurological disease as myotonic dystrophy  </p>

                                 <p>13) Development and structural defects as mild degree of Germinal cell hypo-plasia</p>

                                 <p>14) Partial Androgen resistance  </p>

                                 <p>15) Mycoplasmal infection  </p>

                                 <p>16) Partial Immotile cilia syndrome</p>

                                 <p>17) Drugs (e.g. spironolactone, ketoconazole, cyclophosphamide, estrogen administration, sulfasalazine) </p> 

                                 <p>19) Infection – as bacterial epididimo-orchitis, even in prostatis spermatogenic defect have been noted.</p>

                                 <p>20) Hyper-thermia due to cryptorchidism, Environmental exposure as laptop use or hot working conditions</p>

                                 <p>21) Chromosomal abnormality: has been found in many cases of low sperm count</p>

                                 <p>22) Alcohol use, Cocaine or heavy marijuana use or Tobacco smoking may lower sperm count</p>

                                 <p>23) Anti-sperm antibodies.</p>

                                 <p>24) Klinefelter's syndrome. In this disorder of the sex chromosomes, a man has two X chromosomes and one Y chromosome instead of one X and one Y. This causes abnormal development of the testicles, resulting in low or absent sperm production. Testosterone production also may be lower.</p>

                                <p>25) .Mosaic Klinefelter's syndrome. In this disorder of the sex chromosomes, of the man is abnormal. This causes abnormal development of the testicles, resulting in low sperm production. Testosterone production may be low or normal.</p>

                                 <p>26) Trauma to testis</p>

                                 <p>27) Environmental toxins: as Pesticides and other chemicals in food  or as ayurvedic medicines.</p>

                                 <p>28) Genetic Factors : as idiopathic partial hypo-gonadotropic hypogonadism</p>

                              </div>

                        </div>

                    </div>

                </div>

            </div>



             <div class="col-xl-4 col-lg-4">
                    @include('pages/latest-news')
                    @include('pages/related-diseas')
                    @include('pages/quick-contact')
                </div>

        </div>

    </div>

    </div>



</main>

@endsection