@extends('master.default')

@section('content')



<main>

    <!-- breadcrumb-area -->

    <section class="breadcrumb-area blue-bg">

        <div class="container">

            <div class="row">

                <div class="col-xl-6 col-md-6 mb-15 mt-15">

                    <div class="breadcrumb-title">

                        <h3>About Us</h3>

                    </div>

                </div>

                <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">

                    <div class="breadcrumb">

                        <ul>

                            <li><a href="#">Home</a></li>

                            <li><a href="#">page</a></li>

                            <li><a href="#">about</a></li>

                        </ul>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <!-- about-info -->



    <!-- about-area -->

 <div class="services-area pt-70 pb-65">

        <div class="container">

            <div class="row">

                <div class="col-xl-8 col-lg-8">

                    <div class="row">

                        <div class="col-xl-12">

                            <div class="service-details mb-40">

                                <img class="mb-30" src="{!! asset('front/img/service/details/about.jpg') !!}" alt="">

                                <h3>Dr. M.S. Siddiqui's Introduction</h3>

                                <div class="mid-bar"></div>

                                <p><span class="name">Dr. M.S. Siddiqui</span> born in Barabanki. He has a quality of social working since childhood. He persued his education in Barabanki but his desire to continue his service in the field of Medicine prompted him to join BUMS (Bachelor of Unani Medicine and Surgery).

                                <p>Extra ordinary brilliant as M.S. Siddiqui was, he passed BUMS degree. After that he was selected to pursue his higher studies in the Open International University of Complementry Medicine Srilanka. He got his M.D. (A.M.) degree. Lots of offer started pouring in for him to stay back to abroad and practice there but he refused all of them to return to India and serve the people of India, a place which had given him all the love and recognition. He specialized in the field of curing sexual debility, oligospermia the problem of inadequate sperm count, childless couple & venereal disease during entire educational career he tried to find remedies for above problem.</p>

                                <p>On his way to find suitable cure for erectile dysfunction he developed capsules which are 1000 times better than American drug Viagra. The result of which are very encouraging. For his research Dr. M.S. Siddiqui was appreciated by AYUSH society</p>

                                <p>He has done many certificate research courses like.</p>

                                <p><strong>“Certificate in HIV & Family Education”</strong> started free HIV consultancy & awareness.

                                <p><strong>“P.G. diploma in Yoga Therapy”</strong> Also dictates many sexual yoga asanas which help in sexual debility like “Ashwani Mudra” in PME.</p>

                                <p>Diploma is Acupuncture.Dr. also suggests some acupuncture therapy which beneficial for Neurological disorder.</p>

                                

                            </div>

                        </div>

                    </div>

                </div>

                <div class="col-xl-4 col-lg-4">
                    @include('pages/latest-news')
                    @include('pages/related-diseas')
                    @include('pages/quick-contact')
                </div>

            </div>

        </div>

    </div>

</main>

@endsection