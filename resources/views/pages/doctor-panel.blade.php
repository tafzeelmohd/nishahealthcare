@extends('master.default')
@section('content')
<main>
<!-- breadcrumb-area -->
<section class="breadcrumb-area blue-bg">
<div class="container">
    <div class="row">
        <div class="col-xl-6 col-md-6 mb-15 mt-15">
            <div class="breadcrumb-title">
                <h3>Dr. Ruby Siddiqui</h3>
                <p>Women's Infertility Specialist &amp; Gynaecologist</p>
      
            </div>
        </div>
        <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">
            <div class="breadcrumb">
                <ul>
                    <li><a href="#">Home</a></li>
                    <li><a href="#">page</a></li>
                    <li><a href="#">Dr. Siddiqui</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
</section>
<!-- about-info -->

<!-- about-area -->
<div class="services-area pt-70 pb-65">
<div class="container">
    <div class="row">
        <div class="col-xl-12 col-lg-12">
            <div class="row">
                <div class="col-xl-12">
                    <div class="service-details mb-40">
                        <img class="mb-30" src="{!! asset('front/img/about/topMainPicm0;.jpg') !!}" alt="">
                        <h3>Dr. Ruby Siddiqui (Consultant of morpheus IVF)</h3>
                        <div class="mid-bar"></div>
                        <p><span class="name">Dr. Ruby Siddiqui</span> is a Senior , Well known Women's Infertility Specialist and Gynecologist Consultant in Nisha Health Care , Barabanki.
                        She provides comprehensive and complete solutions to all fertility problems. Her results are excellent and at par with international standards.
                        Specialities :
                        She has a vast experience in handling complicated cases and she specialises in Recurrent Implantation Failure and Poor Responders . The excellent pregnancy rates with frozen oocytes adds to her credentials . She is abreast with latest advancement and technology like PGS / PGD and has given smiles to couples with genetic disorders.
                        Dr. Ruby Siddiqui is a very active academician. </p>

                        <p>Dr. Ruby Siddiqui has won awards on several occasions to appreciate her contribution in the field of Gynecology and Women sex problems. She takes the responsibility upon her to ensure every patient is addressed properly and given proper attention to address the issue in the quickest possible time for quick results. She is a pro in her field and has great knowledge to treat complete spectrum of the gynaecologic problems and conditions.</p>
                </div>
            </div>
         </div>
         <div class="row">
            <div class="col-xs-7 col-lg-7">
                <img class="mb-30 img-fluid" src="{!! asset('front/img/about/blog-nisha-health-care.jpg') !!}" alt=""> 
            </div>
            <div class="col-xs-5 col-lg-5">
                   <h4>Who is a Gynecologist?</h4>
                    <div class="mid-bar"></div>
                    <p>A gynecologist is a doctor who has specialized in woman’s health and female reproductive system. They are the ones woman consult for any kind of health issues: irregular periods, pregnancy, PCOD, etc. As a woman, you must make sure to visit the best gynecologist in Barabanki once in a while to protect yourself from prevailing medical ailments and complex health problems.</p>
                    <div class="contact-person" style="padding: 0;">
                        <p>Dr. Ruby Siddiqui</p>
                        <p>Infertility Specialist & Gynecologist</p>
                        <P>Consultant of morpheus IVF</P>
                        <p>Civil Lines Barabanki-225001 (U.P.)</p>
                        <p><a href="#" class=""><i class="far fa-calendar-alt"></i>Mon - Sat 9:00 AM - 2:00 PM <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5:30 PM - 8:30 PM</a></p>
                    </div>
                </div>
         </div>
    </div>
</div>
</div>
</main>
<style>
.breadcrumb-title p {
margin: 0;
color: #fff;
font-size: 14px;
line-height: 20px;
}.contact-person p {
    margin-bottom: 4px;
}.contact-person P {
    FONT-WEIGHT: 600;
}
</style>
@endsection