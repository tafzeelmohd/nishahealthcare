@extends('master.default')
@section('content')

<main>
    <!-- breadcrumb-area -->
    <section class="breadcrumb-area blue-bg">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-6 mb-15 mt-15">
                    <div class="breadcrumb-title">
                        <h3>Common Diseases</h3>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Service</a></li>
                            <li><a href="#">Common Diseases</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Male Sexual Problems -->

     <section class="related-project pt-50 pb-70">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 text-center pb-40">
                        <div class="section-title service-title">
                            <h2>Common Diseases</h2>
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form,words which don't look even slightly believable.</p>
                        </div>
                    </div>
                </div>
               <div class="row service-shadow">
                   <div class="col-xl-4 col-lg-4 col-md-12 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="{{'piles'}}">
                                    <img src="{!! asset('front/img/common-problems/piles.jpg') !!}" alt="">
                                </a>
                            </div>
                            <div class="link-box home-blog-link text-center">
                                <a href="{{'piles'}}">Piles / Fistula</a>
                            </div>
                        </div>
                    </div>
                     <div class="col-xl-4 col-lg-4 col-md-12 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="{{'menstrual-disorder'}}">
                                      <img src="{!! asset('front/img/common-problems/menstrual.jpg') !!}" alt="">
                                </a>
                            </div>
                            <div class="link-box home-blog-link text-center">
                                <a href="{{'menstrual-disorder'}}">Menstrual Disorder</a>
                            </div>
                        </div>
                    </div>
                     <div class="col-xl-4 col-lg-4 col-md-12 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="{{'diabetes'}}">
                                      <img src="{!! asset('front/img/common-problems/diabetes.jpg') !!}" alt="">
                                </a>
                            </div>
                            <div class="link-box home-blog-link text-center">
                                <a href="{{'diabetes'}}">Diabetes</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-12 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="{{'hairfall'}}">
                                      <img src="{!! asset('front/img/common-problems/hairfall.jpg') !!}" alt="">
                                </a>
                            </div>
                            <div class="link-box home-blog-link text-center">
                                <a href="{{'hairfall'}}">Hair Fall </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-12 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="{{'hairgreing'}}">
                                     <img src="{!! asset('front/img/common-problems/hairgray.jpg') !!}" alt="">
                                </a>
                            </div>
                            <div class="link-box home-blog-link text-center">
                                <a href="{{'hairgreing'}}">Hair Greying</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-12 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="{{'weight-gain'}}">
                                    <img src="{!! asset('front/img/common-problems/weight-gain.jpg') !!}" alt="">
                                </a>
                            </div>
                            <div class="link-box home-blog-link text-center">
                                <a href="{{'weight-gain'}}">Weight Gain</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-12 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="{{'obesity'}}">
                                      <img src="{!! asset('front/img/common-problems/obesity-problem.jpg') !!}" alt="">
                                </a>
                            </div>
                            <div class="link-box home-blog-link text-center">
                                <a href="{{'obesity'}}">Obesity</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-12 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="{{'pimples'}}">
                                      <img src="{!! asset('front/img/common-problems/pimples.jpg') !!}" alt="">
                                </a>
                            </div>
                            <div class="link-box home-blog-link text-center">
                                <a href="{{'pimples'}}">Pimples</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
</main>
@endsection