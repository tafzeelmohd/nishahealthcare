@extends('master.default')
@section('content')

<main>
    <!-- breadcrumb-area -->
    <section class="breadcrumb-area blue-bg">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-6 mb-15 mt-15">
                    <div class="breadcrumb-title">
                        <h3>Female Factor Infertility</h3>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Service</a></li>
                            <li><a href="#">Female Factor Infertility</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Male Sexual Problems -->

                <section class="related-project pt-50 pb-70">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 text-center pb-40">
                        <div class="section-title service-title">
                            <h2>Female Factor Infertility</h2>
                            <p><strong>Important:</strong> All Female Patients will be consulted by Dr. Ruby Siddiqui (Women's Infertility Specialist & Gynaecologist)</p>
                        </div>
                    </div>
                </div>
               <div class="row service-shadow">
                   <div class="col-xl-4 col-lg-4 col-md-12 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="{{'female-infertility'}}">
                                    <img src="{!! asset('front/img/women-problems/infertility.jpg') !!}" alt="">
                                </a>
                            </div>
                            <div class="link-box home-blog-link text-center">
                                <a href="{{'female-infertility'}}">Infertility In Women</a>
                            </div>
                        </div>
                    </div>
                     <div class="col-xl-4 col-lg-4 col-md-12 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="{{'thyroid-disorder'}}">
                                      <img src="{!! asset('front/img/women-problems/thyroid.jpg') !!}" alt="">
                                </a>
                            </div>
                            <div class="link-box home-blog-link text-center">
                                <a href="{{'thyroid-disorder'}}">Thyroid Disorder Includes Infertility</a>
                            </div>
                        </div>
                    </div>
                     <div class="col-xl-4 col-lg-4 col-md-12 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="{{'recurrent-abortion'}}">
                                     <img src="{!! asset('front/img/women-problems/abortion.jpg') !!}" alt="">
                                </a>
                            </div>
                            <div class="link-box home-blog-link text-center">
                                <a href="{{'recurrent-abortion'}}">Recurrent Abortions</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
</main>
@endsection