@extends('master.default')
@section('content')

<main>
    <!-- breadcrumb-area -->
    <section class="breadcrumb-area blue-bg">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-6 mb-15 mt-15">
                    <div class="breadcrumb-title">
                        <h3>Male Sex Problems</h3>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Service</a></li>
                            <li><a href="#">Male Sex Problems</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Male Sexual Problems -->

                <section class="related-project pt-50 pb-70">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 text-center pb-40">
                        <div class="section-title service-title">
                            <h2>Male Sexual Problems</h2>
                            <p>You are not alone! Most men will face one or another kind of sexual disorders.</p>
                        </div>
                    </div>
                </div>
               <div class="row service-shadow">
                   <div class="col-xl-4 col-lg-4 col-md-12 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="{{'premeture-ejeculation'}}">
                                    <img src="{!! asset('front/img/mens-problems/premeture-ejec.jpg') !!}" alt="">
                                </a>
                            </div>
                            <div class="link-box home-blog-link text-center">
                                <a href="{{'premeture-ejeculation'}}">Premature Ejeculation</a>
                            </div>
                        </div>
                    </div>
                     <div class="col-xl-4 col-lg-4 col-md-12 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="{{'erectile-dysfunction'}}">
                                      <img src="{!! asset('front/img/mens-problems/erectile-dys.jpg') !!}" alt="">
                                </a>
                            </div>
                            <div class="link-box home-blog-link text-center">
                                <a href="{{'erectile-dysfunction'}}">Erectile Dysfunction</a>
                            </div>
                        </div>
                    </div>
                     <div class="col-xl-4 col-lg-4 col-md-12 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="{{'nightfall'}}">
                                    <img src="{!! asset('front/img/mens-problems/nightfall.jpg') !!}" alt="">
                                </a>
                            </div>
                            <div class="link-box home-blog-link text-center">
                                <a href="{{'nightfall'}}">Night Fall</a>
                            </div>
                        </div>
                    </div>
                     <div class="col-xl-4 col-lg-4 col-md-12 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="{{'loss-of-libido'}}">
                                     <img src="{!! asset('front/img/mens-problems/loss-of-libido.jpg') !!}" alt="">
                                </a>
                            </div>
                            <div class="link-box home-blog-link text-center">
                                <a href="{{'loss-of-libido'}}">Loss of Libido</a>
                            </div>
                        </div>
                    </div>
                     <div class="col-xl-4 col-lg-4 col-md-12 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="{{'dhat'}}">
                                     <img src="{!! asset('front/img/mens-problems/dhat.jpg') !!}" alt="">
                                </a>
                            </div>
                            <div class="link-box home-blog-link text-center">
                                <a href="{{'dhat'}}">Dhat</a>
                            </div>
                        </div>
                    </div>
                     <div class="col-xl-4 col-lg-4 col-md-12 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="{{'low-testosterone'}}">
                                    <img src="{!! asset('front/img/mens-problems/low-testos.jpg') !!}" alt="">
                                </a>
                            </div>
                            <div class="link-box home-blog-link text-center">
                                <a href="{{'low-testosterone'}}">Low Testosterone & Androgens</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-12 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="{{'infertility'}}">
                                    <img src="{!! asset('front/img/mens-problems/infertility.jpg') !!}" alt="">
                                </a>
                            </div>
                            <div class="link-box home-blog-link text-center">
                                <a href="{{'infertility'}}">Infertility / Ozospermia</a>
                            </div>
                        </div>
                    </div>
                     <div class="col-xl-4 col-lg-4 col-md-12 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="{{'semen-in-urine'}}">
                                     <img src="{!! asset('front/img/mens-problems/urine-semen.jpg') !!}" alt="">
                                </a>
                            </div>
                            <div class="link-box home-blog-link text-center">
                                <a href="{{'semen-in-urine'}}">Semen In Urine</a>
                            </div>
                        </div>
                    </div>
                     <div class="col-xl-4 col-lg-4 col-md-12 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="{{'penis-treatments'}}">
                                    <img src="{!! asset('front/img/mens-problems/penis-trea.jpg') !!}" alt="">
                                </a>
                            </div>
                            <div class="link-box home-blog-link text-center">
                                <a href="{{'penis-treatments'}}">Penis Treatments</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
</main>
@endsection