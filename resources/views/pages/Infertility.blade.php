@extends('master.default')

@section('content')



<main>

    <!-- breadcrumb-area -->

    <section class="breadcrumb-area blue-bg">

        <div class="container">

            <div class="row">

                <div class="col-xl-6 col-md-6 mb-15 mt-15">

                    <div class="breadcrumb-title">

                        <h3>Infertility / Ozospermia</h3>

                    </div>

                </div>

                <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">

                    <div class="breadcrumb">

                        <ul>

                            <li><a href="#">Home</a></li>

                            <li><a href="#">Men's Sex Problems</a></li>

                            <li><a href="#">Infertility / Ozospermia</a></li>

                        </ul>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <!--Premeture Ejeculation Problems -->



    <div class="services-area pt-50 pb-65">

    <div class="container">

        <div class="row">

            <div class="col-xl-8 col-lg-8">

                <div class="row">

                    <div class="col-xl-12">

                        <div class="service-details mb-40">

                            <img class="mb-30" src="{!! asset('front/img/mens-problems/infertility-details.jpg') !!}" alt="">

                            <h3>Infertility / Ozospermia ?</h3>

                            <div class="mid-bar"></div>

                            <p>Infertility primarily refers to the biological inability of a person to contribute to conception. Infertility may also refer to the state of a woman who is unable to carry a pregnancy to full term. There are many biological causes of infertility, some which may be bypassed with medical intervention.



                            <p>Women who are fertile experience a natural period of fertility before and during ovulation, and they are naturally infertile during the rest of the menstrual cycle. Fertility awareness methods are used to discern when these changes occur by tracking changes in cervical mucus or basal body temperature.</p>



                            <p>Couples with primary infertility have never been able to conceive,[5] while, on the other hand, secondary infertility is difficulty conceiving after already having conceived (and either carried the pregnancy to term or had a miscarriage). Secondary infertility is not present if there has been a change of partners (this follows tautologically from the convention of speaking of couples, rather than individuals, as being infertile; if there is a change of partners, then a new couple is created, with its own chances to be infertile.</p>

                            <br>



                            <h3>CAUSES</h3>

                              <div class="mid-bar"></div>

                                <div class="service-details mt-30 mb-40">

                                <h6><strong>Further information: Semen quality</strong></h6>

                                <p>There are many causes for oligospermia including:Pre-testicular causes. Pre-testicular factors refer to conditions that impede adequate support of the testes and include situations of poor hormonal support and poor general health including: a. Hypogonadism due to various causes b. Drugs, alcohol, smoking c. Medications, including androgens.</p>



                                <h6><strong>Infertility Causes in male & female</strong></h6>

                                <p>A diagnosis of infertility is often considered in two stages, the first stage being the general infertility diagnosis, and the second stage being a diagnosis of the specific cause of the infertility. Infertility is almost a symptom itself. To treat infertility in the best possible way, finding the cause is helpful.</p>





                                <h6><strong>Two of the most common causes of female factor infertility include:</strong></h6>

                                <p>Ovulatory Disorders – accounting for 18% to 30% of infertility in women

                                Anatomical Disorders – like blocked fallopian tubes, often the result of infections or inflammations, like endometriosis or pelvic inflammatory disease</p>



                                <h6><strong>The two most common causes of male factor infertility include:</strong></h6>

                                <p>Low Sperm Production: Medically referred to as azoospermia (complete lack of sperm cells) or oligospermia (few sperm cell are produced), this is the most common cause of male factor infertility

                                Sperm Malformations: Though the amount of sperm is within the normal range, malformed sperm, as well as the inability of the sperm to make it to the egg before dying, can be a cause of infertility. Whatever causes infertility, the person should know there is reason for hope.</p>

                              </div>

                        </div>

                    </div>

                </div>

            </div>



            <div class="col-xl-4 col-lg-4">
                    @include('pages/latest-news')
                    @include('pages/related-diseas')
                    @include('pages/quick-contact')
                </div>
        </div>

    </div>

    </div>



</main>

@endsection