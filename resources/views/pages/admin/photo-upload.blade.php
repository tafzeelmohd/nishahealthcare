@extends('master.default')
@section('content')
<main>

<!-- breadcrumb-area -->
<section class="breadcrumb-area blue-bg">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-md-6 mb-15 mt-15">
                <div class="breadcrumb-title">
                    <h3>Upload Photos</h3>
                </div>
            </div>

            <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">
                <div class="breadcrumb">
                     <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('photos') }}">
                                       My Photos
                                    </a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

 <!-- about-info -->

 <div class="services-area pt-70 pb-65">

      <div class="container">

          @if ($message = Session::get('error'))
            <div class="alert alert-warning border-0 alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <span class="font-weight-semibold">Warning!</span> {{$message}} <a href="#" class="alert-link"></a>, Please try again.
            </div>
            @endif

            @if ($message = Session::get('success'))
            <div class="alert alert-success border-0 alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <span class="font-weight-semibold">Well done! </span> {{$message}}<a href="javascript:;" class="alert-link"> on records</a>
            </div>
          @endif

        <form action="{{route('uploadPhoto')}}" method="POST" name="photo-upload" enctype="multipart/form-data">
                    
        <div class="row">
          <div class="col-md-6 offset-md-3">
          <div class="card">
            <div class="card-header">Upload Your Photo</div>
            <div class="card-body">
            <label for="first_name" class="col-form-label">Photo Title</label>
            @csrf
                <input id="first_name" type="text" class="form-control" name="title" value="{{ old('title') }}"
                  placeholder="Enter Photo Title">
                  @if($errors->has('title'))
                    <label id="email-error" class="validation-invalid-label">{{ $errors->first('title') }}</label>
                  @endif
                
                <label for="image" class="col-form-label">Upload Photo </label>
                 
                      <input id="image" type="file" class="form-control" name="image" value="{{ old('image') }}" >
                         @if ($errors->has('image'))
                            <label id="image-error" class="validation-invalid-label">{{ $errors->first('image') }}</label>
                        @endif
                
                   <p id="error1" style="display:none; color:#FF0000;">
                    Invalid Image Format! Image Format Must Be JPG, JPEG, PNG or PDF.
                    </p>
                    <p id="error2" style="display:none; color:#FF0000;">
                    Maximum File Size Limit is 1MB.
                    </p>
                    <br><br>
                    <div class="col-md-12 text-center">
                      <button type="submit" class="btn btn-primary submitBtn" style="width: 150px;">
                       Submit
                    </button></div>
              </div></div>
            </div>
          </div>
            <br>
          
        </form>
      </div>
    </div>
  </main>
  @endsection
<style type="text/css">
    /*.col-md-6.offset-md-3 {
    border: 1px solid #be1c2b47 !important; 
    padding: 20px 45px;
}*/label.col-form-label {
    font-weight: 600;
    color: #000 !important;
}
input#first_name {
    margin-bottom: 11px;
}input {
    height: 45px;
}.btn:hover {
    background: #020d26e0 !important;
    border: 1px solid #fff !important;
    color: #fff;
}label#image-error {
    color: #ff0000;
}a.dropdown-item {
    color: #000 !important;
}
  </style>