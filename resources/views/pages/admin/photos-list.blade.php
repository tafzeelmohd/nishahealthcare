@extends('master.default')
@section('content')
<main>

<!-- breadcrumb-area -->
<section class="breadcrumb-area blue-bg">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-md-6 mb-15 mt-15">
                <div class="breadcrumb-title">
                    <h3>Upload Photos</h3>
                </div>
            </div>

            <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">
                <div class="breadcrumb">
                     <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

 <!-- about-info -->

 <div class="services-area pt-70 pb-65">

      <div class="container">
        <div class="col-md-8 offset-md-2">

          @if ($message = Session::get('error'))
            <div class="alert alert-warning border-0 alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <span class="font-weight-semibold">Warning!</span> {{$message}} <a href="#" class="alert-link"></a>, Please try again.
            </div>
            @endif

            @if ($message = Session::get('success'))
            <div class="alert alert-success border-0 alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <span class="font-weight-semibold">Well done! </span> {{$message}}<a href="javascript:;" class="alert-link"> on records</a>
            </div>
          @endif

          <table class="table table-striped table-bordered">
              <thead>
                <tr>
                 <!--  <th scope="col">S.No</th> -->
                  <th scope="col">Photo</th>
                  <th scope="col">Photo Title</th>
                  <th scope="col" style="width: 20%">Action</th>
                </tr>
              </thead>
              <tbody>
                @forelse($photos as $key => $photo)
                <tr>
                  <!-- <th scope="row">{{$key+1}}</th> -->
                  <td>
                     <div class="portfolio-wrapper" style="width: 120px;">
                        <div class="portfolio-thumb portfolio-popup">
                        <img src="{!! asset('front/upload/gallary/'.$photo->image) !!}" class="img-fluid" alt="">
                         <a href="{!! asset('front/upload/gallary/'.$photo->image) !!}" class="view"><i class="icofont-plus"></i></a>
                         </div>
                    </div>
                    </td>
                       
                  <td>{{(!empty($photo->titile)) ? $photo->title : "Not Available"}}</td>
                  <td>
                    <a href="{{route('photo-delete', ['id' => $photo->id])}}" class="btn btn-action btn-danger">Delete</a>
                  </td>
                </tr>
                @empty
                <tr>
                    <td colspan="3"><center>No PHoto Found</center></td>
                </tr>
                @endforelse
              </tbody>
            </table>

      {{$photos->links()}}
      </div>
  </div>
    </div>
  </main>
  @endsection

  <style>
      a.btn {
    padding: 15px;
}
    ul.pagination li {
    float: left;
}.page-item.active .page-link {
    z-index: 1;
    color: #fff;
    background-color: #be1c2b !important;
    border-color: #be1c2b !important;
}.page-link { color: #000; }
a.btn.btn-action.btn-info {
    background: cadetblue;
}

a.btn.btn-action.btn-danger {
    background: #ce2323;
}
  </style>