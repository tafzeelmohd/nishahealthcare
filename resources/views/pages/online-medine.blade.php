@extends('master.default')
@section('content')

<main>
        <!-- breadcrumb-area -->
        <section class="breadcrumb-area blue-bg">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-md-6 mb-15 mt-15">
                        <div class="breadcrumb-title">
                            <h3>Online Medicine</h3>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">
                        <div class="breadcrumb">
                            <ul>
                                <li><a href="#">Home</a></li>
                                <li><a href="#">About</a></li>
                                <li><a href="#" class="active">Online Medicine</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- about-info -->

        <!-- about-area -->
     <div class="services-area pt-70 pb-25 bg-theme">
            <div class="container">
                <div class="row">
                    <div class="col-12 mb-45">
                        <div class="group-contact row">
                    <div class="col" style="padding:0;">
                        <div class="group-form-contact">
                                                                        
                            <form action="javascript:;" class="contactform contact-form" accept-charset="utf-8">
                                <div class="title-contact">
                                    <h2>Fill the Deatils</h2>
                                    <div class="sub-form">
                                       Please fill up Your Details to get Medicine By Post
                                    </div>
                                </div>
                                <p class="contact-name">                                      
                                    <input type="text" size="30" placeholder="Your name" name="name" id="name">
                                 </p>
                                <p class="contact-form-email">          
                                    <input type="text" size="12" placeholder="Your Phone No" name="phone" id="phone">
                                </p> 
                                <p class="contact-form-comment">
                                    <textarea id="address" class="comment-messages" placeholder="Address" tabindex="4" name="address"></textarea>
                                </p>  
                                <div class="btn-contact">
                                    <p class="form-contact">                 
                                        <!--<button type="submit" name="submit" class="educa-button">SEND MESSAGE</button>-->
                                        <input type="submit" name="submit" class="btn" value="Submit">
                                       <!--  <a class="btn" href="#">Submit</a> -->
                                    </p>
                                </div>        
                            </form>
                        </div>      
                    </div>
                    <div class="col" style="padding-left: 0; background: #fff;">
                        <div class="group-info-contact">
                            <div class="title-contact">
                                <h2>Bank Account Details</h2>
                            </div>      
                
                            <ul class="info-adrress">
                                <li><strong>Bank Name:</strong> Bank Of Baroda</li>
                                <li><strong>Account Name: </strong><br> Nisha Health Care Clinic and Research Hospital</li>
                                <li><strong>Account Number:</strong> <br> 25150200010641</li>
                                <li><strong>Ifsc Code:</strong> <br>BARBOBARBAN</li>
                            </ul>
                            <hr>
                            <div class="call-direc">
                                Contact Details:<br>
                                Phone: <a href="javascript:;">(+91) 9839467283</a><br>
                                Email Id: <a href="javascript:;" class="mail-contact">nishahealthcare0786@gmail.com</a>
                            </div>
                        </div>
                    </div>
                </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <style type="text/css">
    hr {
    margin: 5px 0;
}
        .section-onehalf {
    float: left;
    width: 50%;
}
.group-form-contact:before {
    position: absolute;
    content: "";
    width: 100%;
    height: 100%;
    background: #000;
    opacity: .7;
}
.group-form-contact {
    background: url('http://jpninfo.com/wp-content/uploads/2017/05/medicine.jpg')no-repeat;
    -webkit-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
    position: relative;
}.contactform {
    padding: 15px 115px 20px 100px;
    position: relative;
}.contactform .title-contact {
    margin-bottom: 25px;
    color: #fff; position: relative;
}group-contact .group-info-contact {
    padding-left: 60px !important;
}
.group-contact .group-info-contact {
    padding: 33px 0px 45px 60px;
}
 .group-contact {
    box-shadow: 0px 18px 32px 0px rgba(0, 0, 0, 0.1);
}.bg-theme {
    background-color: #f7f7f7;
}
.educa-row {
    clear: both;
    display: block;
    position: relative;
    padding: 100px 0;
}.group-contact.clearfix {
    background: #fff;
}.group-info-contact .title-contact h2 {
    margin-bottom: 7px;
    color: #fff;
}
.contactform h2 {
    color: #fff !important;
}
ul.info-adrress li {
    margin-bottom: 6px;
}input.btn {
    color: #fff;
    padding: 12px 0;
}
.group-contact .title-contact h2 {
    font-weight: 500;
    font-size: 24px;
    /* text-transform: uppercase; */
    margin-bottom: 15px;
    color: #002447;
}.group-info-contact .info-adrress {
    margin-bottom: 18px;
}
.group-info-contact .info-adrress li, .group-info-contact .call-direc, .group-info-contact .contact-link p {
    font-size: 16px;
    line-height: 30px;
    color: #1a1a23;
}
.call-direc a { color: #be1c2b;  font-size: 13px;}
    </style>
@endsection