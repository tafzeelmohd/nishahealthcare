<div class="contact-sidebar mb-50">
    <div class="contact-form sidebar-link grey-bg">
       <h3><i class="fas fa-headphones-alt"></i>&nbsp;&nbsp;Enquiry</h3>
       <form action="{{url('patient/mail')}}" method="post">
        <div class="inner-pad">
        <div class="col-xl-12 mb-20">
            <input name="name" type="text" placeholder="Full Name" required="">
           <input type="hidden" name="_token" value="{{ csrf_token() }}">

        </div>
        
        <div class="col-xl-12 mb-20">
            <input name="contact" type="text" placeholder="Contact Number" required="">
        </div>

        <div class="col-xl-12 mb-20">
            <input name="problem" type="text" placeholder="Problem" required="">
        </div>

        <div class="col-xl-12 text-center">
            <button class="btn" type="submit">Submit</button>
        </div>
        </div>
        </form>
    </div>
</div>
