@extends('master.default')

@section('content')



<main>

    <!-- breadcrumb-area -->

    <section class="breadcrumb-area blue-bg">

        <div class="container">

            <div class="row">

                <div class="col-xl-6 col-md-6 mb-15 mt-15">

                    <div class="breadcrumb-title">

                        <h3>Contact Us</h3>

                    </div>

                </div>

                <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">

                    <div class="breadcrumb">

                        <ul>

                            <li><a href="javascript:;">Home</a></li>

                            <li><a href="javascript:;">Contact-us</a></li>

                        </ul>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <!--Painfull Intercourse Problems -->



    <div class="services-area pt-50 pb-65">

    <div class="container">

        <div class="row">



        <section class="contact-form pb-50">

            <div class="container">

                <div class="row">

                    <div class="col-12 text-center">

                        <div class="contact-form-title">

                            <h2>Get in Touch with us</h2>

                            <p>We are a dedicated and a genuine healthcare center to eradicate all your sexual ailments with our team of expertise and our high-end medications.</p>

                        </div>

                    </div>

                </div>

                <div class="row">
                    <div class="col-md-8">
                          <form id="contact-form" action="{{url('patient/mail')}}" class="contact-form" method="POST">
                            <div class="row">
                                <div class="col-xl-6 col-lg-6 mb-20">
                                    <input name="name" type="text" placeholder="Name">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" required="">
                                </div>
                                <div class="col-xl-6 col-lg-6 mb-20">
                                    <input name="email" type="email" placeholder="Email" required="">
                                </div>
                                <div class="col-xl-12 mb-20">
                                    <input name="contact" type="text" placeholder="Phone" required="">
                                </div>
                                <div class="col-xl-12 mb-20">
                                    <textarea name="problem" id="text-message" cols="30" rows="10" placeholder="Message*" required=""></textarea>
                                </div>
                                <div class="col-xl-12 text-center">
                                    <button class="btn" type="submit">Send Mail</button>
                                </div>
                            </div>
                        </form>
                        <p class="ajax-response"></p>
                    </div>



                     <div class="col-md-4 text-center text-md-left mb-30">
                        <div class="contact-person">
                            <h4>Nisha Health Care Clinic</h4>
                            <p>Dr. M.S. Siddiqui</p>
                            <p>M.D.(AM),BUMS,CHFE,DAP,PGDYT,FIANT
                            Ghosiyana , Station Road ( Near - Ultra Pathalogy)</p>
                            <p>Civil Lines Barabanki-225001(U.P.)</p>
                            <p><a href="#" class=""><i class="far fa-calendar-alt"></i>Mon - Sat 9:00 am - 2:00 PM <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5:30 pm - 8:30 PM</a></p>
                            <p><a href="#" class=""><i class="fas fa-phone"></i>(+91) +91- 9839467283</a></p>
                            <p><a href="#" class=""><i class="far fa-envelope"></i>nishahealthcare0786@gmail.com</a></p>
                        </div>
                    </div>
                </div>

            </div>

        </section>

        </div>

    </div>

    </div>



</main>

@endsection