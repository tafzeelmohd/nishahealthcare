@extends('master.default')

@section('content')



<main>

    <!-- breadcrumb-area -->

    <section class="breadcrumb-area blue-bg">

        <div class="container">

            <div class="row">

                <div class="col-xl-6 col-md-6 mb-15 mt-15">

                    <div class="breadcrumb-title">

                        <h3>Menstrual disorder</h3>

                    </div>

                </div>

                <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">

                    <div class="breadcrumb">

                        <ul>

                            <li><a href="#">Home</a></li>

                            <li><a href="#">Common Diseases</a></li>

                            <li><a href="#">Menstrual disorder</a></li>

                        </ul>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <!--Premeture Ejeculation Problems -->



    <div class="services-area pt-50 pb-65">

    <div class="container">

        <div class="row">

            <div class="col-xl-8 col-lg-8">

                <div class="row">

                    <div class="col-xl-12">

                        <div class="service-details mb-20">

                            <img class="mb-30" src="{!! asset('front/img/common-dis/menstrual-disorder.jpg') !!}" alt="">

                            <br>

                            <h3>Menstrual Disorder ?</h3>

                            <div class="mid-bar"></div>

                            <p>Menstruation disorders are a common problem during adolescence. These disorders may cause significant anxiety for patients and their families.[1] Physical and psychological factors contribute to the problem. In order to treat menstruation disorders, becoming familiar with the normal menstrual cycle is important.</p>



                            <p>For a regular menstrual cycle, the median age of menarche is 12.77 years. The average interval between thelarche and menarche is about 2 years, and 90% of females menstruate by the time they have Tanner IV breast and pubic hair development. Most cycles occur between 21-35 days with 3-10 days of bleeding and 30-40 mL of blood loss. Anovulatory cycles and irregular menstrual patterns are common within 24 months of menarche.</p>



                            <p><strong>Classification of menstrual disorders</strong></p>



                            <p>Attempts are currently underway to establish a standardized international nomenclature for menstrual disorders. The existing broad classification is as follows:</p>

                            <p>Amenorrhea and oligomenorrhea (lack of bleeding or too little bleeding)</p>

                            <p>Dysmenorrhea (painful menstruation)</p>

                            <p>Menorrhagia (excessive bleeding)</p>

                            <p>Amenorrhea</p>



                            <p>Amenorrhea may be primary (ie, never menstruated) or secondary (ie, menarche, but no periods for 3 consecutive months). Primary amenorrhea is the absence of menstruation by age 16 years in the presence of normal pubertal development or by age 14 years in the absence of normal pubertal development. Evaluating for breast and uterine development in patients with a menstruation disorder is important. Secondary amenorrhea is more common than primary amenorrhea. The most common etiology is dysfunction of the hypothalamic-pituitary-ovarian (HPO) axis.</p>

                            

                            <p><strong>Dysmenorrhea</strong></p>

                            <p>Dysmenorrhea is a very common complaint and may be primary or secondary, although primary dysmenorrhea is more prevalent. Symptoms include crampy lower abdominal and pelvic pain that radiates to the thighs and back without associated pelvic pathology. Dysmenorrhea is caused by prostaglandins and leukotrienes during ovulatory cycles. Endometrial prostaglandin levels increase during the luteal and menstrual phases of the cycle, causing uterine contractions. Secondary dysmenorrhea is rare, and pain is associated with pelvic pathology (eg, bicornuate uterus, endometriosis, pelvic inflammatory disease, uterine fibroids). An underlying pelvic pathology (eg, endometriosis) or an uterine anomaly (eg, fibroids) may be present in about 10% of severe dysmenorrhea cases.</p>

                            <p>MenorrhagiaMenstrual bleeding that lasts more than 8-10 days with blood loss of over 80 mL is considered excessive.</p>

                            

                            </div>

                        </div>

                    </div>

                </div>

          



            <div class="col-xl-4 col-lg-4">
                    @include('pages/latest-news')
                    @include('pages/related-diseas')
                    @include('pages/quick-contact')
                </div>
        </div>

    </div>

    </div>



</main>

@endsection