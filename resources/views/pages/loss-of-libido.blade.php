@extends('master.default')

@section('content')



<main>

    <!-- breadcrumb-area -->

    <section class="breadcrumb-area blue-bg">

        <div class="container">

            <div class="row">

                <div class="col-xl-6 col-md-6 mb-15 mt-15">

                    <div class="breadcrumb-title">

                        <h3>Loss of Libido</h3>

                    </div>

                </div>

                <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">

                    <div class="breadcrumb">

                        <ul>

                            <li><a href="#">Home</a></li>

                            <li><a href="#">Men's Sex Problems</a></li>

                            <li><a href="#">Loss of Libido</a></li>

                        </ul>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <!--Premeture Ejeculation Problems -->



    <div class="services-area pt-50 pb-65">

    <div class="container">

        <div class="row">

            <div class="col-xl-8 col-lg-8">

                <div class="row">

                    <div class="col-xl-12">

                        <div class="service-details mb-40">

                            <img class="mb-30" src="{!! asset('front/img/mens-problems/loss-of-libido-details.jpg') !!}" alt="">

                            <h3>Loss of Libido ?</h3>

                            <p>Libido refers to a person's sex drive or desire for sexual activity. The desire for sex is an aspect of a person's sexuality, but varies enormously from one person to another, and it also varies depending on circumstances at a particular time. Sex drive has usually biological, psychological, and social components. Biologically, levels of hormones such as testosterone are believed to affect sex drive; social factors, such as work and family, also have an impact; as do internal psychological factors, like personality and stress. Sex drive may be affected by medical conditions, medications, lifestyle and relationship issues. A person who has extremely frequent or a suddenly increased sex drive may be experiencing hypersexuality, but there is no measure of what is a healthy level for sex.</p>



                            <p>A person may have a desire for sex but not have the opportunity to act on that desire, or may on personal, moral or religious reasons refrain from acting on the urge. Psychologically, a person's urge can be repressed or sublimated. On the other hand, a person can engage in sexual activity without an actual desire for it. Males reach the peak of their sex drive in their teens, while women reach it in their thirties.</p>

                            <br>

                            <h3>FACTORS AFFECTING LIBIDO</h3>

                                <div class="mid-bar"></div>

                                <div class="service-details mt-30 mb-40">

                                    <h6><strong>Relational issues</strong></h6>

                                    <p>The desire for sex is an important motivator for the formation and maintenance of intimate relationships in both men and women, and a lack or loss of sexual desire can have an adverse impact on a relationship. Unresolved relationship problems, such as a lack or loss of sexual desire for the partner, may cause a decrease in sexual desire, which may itself cause problems in the relationship. Infidelity may be an indication of a general desire for sex, though not with the primary partner. Problems can arise from the loss of sexual desire in general or for the partner or a lack of connection with the partner, or poor communication of sexual needs and preferences.</p>



                                    <h6><strong>Psychological factors</strong></h6>

                                    <p>Psychological factors can reduce the desire for sex. These factors can include lack of privacy and/or intimacy, stress or fatigue, distraction or depression. Environmental stress, such as prolonged exposure to elevated sound levels or bright light, can also affect libido. Other causes include experience of sexual abuse, assault, trauma, or neglect, body image issues and sexual performance anxiety.</p>

                                    <p>Some people have suggested that contraception may influence the desire for sex by women, by decreasing the anxiety level from an unexpected pregnancy. Latent homosexuality may also be a cause for lack of libido in men.</p>





                                    <h6><strong>Physical factors</strong></h6>

                                    <p>Physical factors that can affect libido include: endocrine issues such as hypothyroidism, levels of available testosterone in the bloodstream of both women and men, the effect of certain prescription medications (for example flutamide), various lifestyle factors and the attractiveness and biological fitness of one's partner.Inborn lack of sexual desire, often observed in asexual people, can also be considered a physical factor.</p>



                                    <h6><strong>Lifestyle</strong></h6>

                                    <p>Being very underweight or malnourished can cause a low libido due to disruptions in normal hormonal levels. There is also evidence to support that specific foods have an effect on libido.</p>

                                    <p>Anemia is particularly a cause of lack of libido in women due to the loss of iron during the period.</p>

                                    <p>Smoking, alcohol abuse and drug abuse may also cause disruptions in the hormonal balances and therefore leads to a decreased libido. However, specialists suggest that several lifestyle changes such as drinking milk, exercising, quitting smoking, lower consumption of alcohol or using prescription drugs may help increase one's sexual desire. Moreover, learning stress management techniques can be helpful for individuals who experience libido impairment due to a stressful life.</p>



                                    <p>Aphrodisiacs are known to increase individuals' libido due to either their chemical composition or their consistency.</p>



                                    <h6><strong>Medications</strong></h6>

                                    <p>Reduced libido is also often iatrogenic and can be caused by many medications, such as hormonal contraception, SSRIs and other antidepressants, antipsychotics, opioids and beta blockers. In some cases iatrogenic impotence or other sexual dysfunction can be permanent, as in post-SSRI sexual dysfunction (PSSD).</p>

                                    <p>Testosterone is one of the hormones controlling libido in human beings. Emerging research is showing that hormonal contraception methods like "the pill" (which rely on estrogen and progesterone together) are causing low libido in females by elevating levels of sex hormone binding globulin (SHBG). SHBG binds to sex hormones, including testosterone, rendering them unavailable. Research is showing that even after ending a hormonal contraceptive method, SHBG levels remain elevated and no reliable data exists to predict when this phenomenon will diminish.</p>



                                    <h6><strong>Testosterone and menstrual cycle</strong></h6>

                                    <p>The testosterone and menstrual cycle is alos a factor in loss of libido.</p>



                                </div>

                        </div>

                    </div>

                </div>

            </div>



            <div class="col-xl-4 col-lg-4">
                    @include('pages/latest-news')
                    @include('pages/related-diseas')
                    @include('pages/quick-contact')
                </div>

        </div>

    </div>

    </div>



</main>

@endsection