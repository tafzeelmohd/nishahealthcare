@extends('master.default')

@section('content')



<main>

    <!-- breadcrumb-area -->

    <section class="breadcrumb-area blue-bg">

        <div class="container">

            <div class="row">

                <div class="col-xl-6 col-md-6 mb-15 mt-15">

                    <div class="breadcrumb-title">

                        <h3>Infertility in women</h3>

                    </div>

                </div>

                <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">

                    <div class="breadcrumb">

                        <ul>

                            <li><a href="#">Home</a></li>

                            <li><a href="#">Female Factor Infertility</a></li>

                            <li><a href="#">Infertiility in women</a></li>

                        </ul>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <!--Premeture Ejeculation Problems -->



    <div class="services-area pt-50 pb-65">

    <div class="container">

        <div class="row">

            <div class="col-xl-8 col-lg-8">

                <div class="row">

                    <div class="col-xl-12">

                        <div class="service-details mb-20">

                            <img class="mb-30" src="{!! asset('front/img/male-factor-infertility/dead-sperm.jpg') !!}" alt="">

                            <br>

                            <h3>Infertility in women ?</h3>

                            <div class="mid-bar"></div>

                            <p>Infertility is defined as inability to conceive in a couple having unprotected intercourse for over one year. 

                            Being born a human is blessing, and able to reproduce is double blessing. In our society to a woman, unable to bear a child is an unbearable suffering. And suffering knows no barrier, no caste or creed no religion or region, that is living suffering, the suffering of human race. Without children, loss of fertility may the loss of women's hope for future. Infertility/childlessness causes, great personal suffering and distress. Most of the agony and misery is hidden from the public gaze, and that is why this topic is still not talked about openly. The reasons for the lack of public support for the childless couple include the dismal ignorance about the causes of infertility and its treatment. Infertile couples are socially isolated and emotionally very vulnerable. More and more couple is showing up at fertility clinics these days, searching for answers, hope and eventual parenthood. All most all the couples expect to have their own babies, once they get married.</p>



                            <p>The commonest reason for this, is what modern medicine/allopathic calls "idiopathic" which simply means we do not know. In allopathic this is one on the reasons why the diagnosis of male infertility is so frustrating for both patients and doctors of modern medicines. In modern medicine medical treatment for male infertility does not have a high success rate and has unpleasant side effects. This simply reflects the modern doctor's ignorance about male infertility. They know very little about what causes it, and their knowledge about how to treat it is even more pitiable. Unexplained infertility simply means the doctors do not know why the couple is infertile. It is a confession of ignorance of modern medicine. </p>

                                

                            </div>

                        </div>

                    </div>

                </div>

        

                 <div class="col-xl-4 col-lg-4">
                    @include('pages/latest-news')
                    @include('pages/related-diseas')
                    @include('pages/quick-contact')
                </div>


        </div>

    </div>

    </div>



</main>

@endsection