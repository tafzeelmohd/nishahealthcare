@extends('master.default')
@section('content')
<div class="container">
    <div class="row justify-content-center" style="padding: 50px 0;">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Execute Artisan Commmand') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('artisan.command') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Enter Artisan Command') }}</label>

                            <div class="col-md-6">
                                <input id="call" type="text" class="form-control" name="command_line" required>

                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-auth btn-primary">
                                    {{ __('Execute') }}
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
