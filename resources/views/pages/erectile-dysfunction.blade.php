@extends('master.default')

@section('content')



<main>

    <!-- breadcrumb-area -->

    <section class="breadcrumb-area blue-bg">

        <div class="container">

            <div class="row">

                <div class="col-xl-6 col-md-6 mb-15 mt-15">

                    <div class="breadcrumb-title">

                        <h3>Erectile Dysfunction</h3>

                    </div>

                </div>

                <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">

                    <div class="breadcrumb">

                        <ul>

                            <li><a href="#">Home</a></li>

                            <li><a href="#">Men's Sex Problems</a></li>

                            <li><a href="#">Erectile Dysfunction</a></li>

                        </ul>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <!--Premeture Ejeculation Problems -->



    <div class="services-area pt-50 pb-65">

    <div class="container">

        <div class="row">

            <div class="col-xl-8 col-lg-8">

                <div class="row">

                    <div class="col-xl-12">

                        <div class="service-details mb-40">

                            <img class="mb-30" src="{!! asset('front/img/mens-problems/erectile-dys-details.jpg') !!}" alt="">

                            <h3>Erectile Dysfunction ?</h3>

                            <p>Erectile dysfunction (impotence) is the inability to get and keep an erection firm enough for sex .</p>

                            <p>Having erection trouble from time to time isn't necessarily a cause for concern. If erectile dysfunction is an ongoing issue, however, it can cause stress, affect your self-confidence and contribute to relationship problems. Problems getting or keeping an erection can also be a sign of an underlying health condition that needs treatment and a risk factor for heart disease.</p>

                            <p>If you're concerned about erectile dysfunction, talk to your doctor — even if you're embarrassed. Sometimes, treating an underlying condition is enough to reverse erectile dysfunction. In other cases, medications or other direct treatments might be needed.</p>

                        </div>

                          <div class="row">

                            <div class="col-xl-6 col-lg-12">

                                <div class="s-details-img mb-30">

                                    <img src="{!! asset('front/img/mens-problems/happy-merr.jpg') !!}" alt="">

                                </div>

                            </div>

                            <div class="col-xl-6 col-lg-12">

                                <div class="service-details mb-40">

                                    <h5 class="green-head">Typical Symptoms:</h5>

                                    <div class="mid-bar"></div>

                                    <ul class="list1">

                                      <li><i class="fa fa-check" aria-hidden="true"></i>Erectile dysfunction symptoms might include persistent:</li>

                                      <li><i class="fa fa-check" aria-hidden="true"></i>Trouble getting an erection</li>

                                      <li><i class="fa fa-check" aria-hidden="true"></i>Trouble keeping an erection</li>

                                      <li><i class="fa fa-check" aria-hidden="true"></i>Reduced sexual desire</li>

                                    </ul>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>



                 <div class="col-xl-4 col-lg-4">
                    @include('pages/latest-news')
                    @include('pages/related-diseas')
                    @include('pages/quick-contact')
                </div>



            

        </div>

    </div>

    </div>



</main>

@endsection