@extends('master.default')

@section('content')



<main>

    <!-- breadcrumb-area -->

    <section class="breadcrumb-area blue-bg">

        <div class="container">

            <div class="row">

                <div class="col-xl-6 col-md-6 mb-15 mt-15">

                    <div class="breadcrumb-title">

                        <h3>Absent Orgasm</h3>

                    </div>

                </div>

                <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">

                    <div class="breadcrumb">

                        <ul>

                            <li><a href="#">Home</a></li>

                            <li><a href="#">Women's Sex Problems</a></li>

                            <li><a href="#">Absent Orgasm</a></li>

                        </ul>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <!--Premeture Ejeculation Problems -->



    <div class="services-area pt-50 pb-65">

    <div class="container">

        <div class="row">

            <div class="col-xl-8 col-lg-8">

                <div class="row">

                    <div class="col-xl-12">

                        <div class="service-details mb-20">

                            <img class="mb-30" src="{!! asset('front/img/women-problems/absent-orgasm-details.jpg') !!}" alt="">

                            <br>

                            <h3>Lack of Orgasm / Sexual Dysfunction ?</h3>

                            <div class="mid-bar"></div>

                            <p>Female sexual dysfunction is a problem during any phase of the sexual response cycle that prevents a woman from experiencing satisfaction from the sexual activity. Causes can be either physical or psychological.</p>

                            

                            <br>

                            <h3>What is sexual dysfunction?</h3>

                              <div class="mid-bar"></div>

                                <div class="service-details mt-10 mb-40">

                                <p>Sexual dysfunction refers to a problem during any phase of the sexual response cycle that prevents the individual or couple from experiencing satisfaction from the sexual activity.</p>



                                <p>Similarly, even if your sex drive is weaker than it once was, your relationship may be stronger than ever. Bottom line: There is no magic number to define low sex drive. It varies from woman to woman.</p>



                                <p>Some signs and symptoms that may indicate a low sex drive include a woman who:</p>



                                <ul class="list2">

                                    <li>Has no interest in any type of sexual activity, including masturbation</li>

                                    <li>Doesn't have sexual fantasies or thoughts, or only seldom has them</li>

                                    <li>Is bothered by her lack of sexual activity or fantasies</li>

                                </ul>

                                <br><br>

                                <h3><strong>When to see a doctor:</strong></h3>

                                <div class="mid-bar"></div>

                                <p>If you're bothered by your low desire for sex, talk to your doctor. The solution could be as simple as changing the type of antidepressant you take.</p>

                                <br>



                                <h3><strong>How does sexual dysfunction affect women?:</strong></h3>

                                <div class="mid-bar"></div>

                                <p>The most common problems related to sexual dysfunction in women include:</p>

                                <ul class="list2">

                                    <li><strong>Inhibited sexual desire:</strong> Inhibited sexual desire is a lack of sexual desire or interest in sex. Many factors can contribute to a lack of desire, including hormonal changes, medical conditions and treatments (for example cancer and chemotherapy), depression, pregnancy, stress and fatigue. Boredom with regular sexual routines also may contribute to a lack of enthusiasm for sex, as can lifestyle factors, such as careers and the care of children.</li>



                                    <li><strong>Inability to become aroused:</strong> For women, the inability to become physically aroused during sexual activity often involves insufficient vaginal lubrication. The inability to become aroused also may be related to anxiety or inadequate stimulation. In addition, researchers are investigating how blood flow disorders affecting the vagina and clitoris may contribute to arousal problems.</li>



                                    <li><strong>Lack of orgasm (anorgasmia):</strong>  The lack of orgasm is the delay or absence of sexual climax (orgasm). It can be caused by sexual inhibition, inexperience, lack of knowledge and psychological factors such as guilt, anxiety, or a past sexual trauma or abuse. Other factors contributing to anorgasmia include insufficient stimulation, certain medications and chronic diseases.</li>



                                    <li><strong>Surgery:</strong>

                                    Pain during intercourse (dyspareunia) can be caused by a number of problems, including endometriosis, pelvic mass, ovarian cysts, inflammation of the vagina (vaginitis), poor lubrication, the presence of scar tissue from surgery and a sexually transmitted disease. A condition called vaginismus is a painful, involuntary spasm of the muscles that surround the vaginal entrance. It may occur in women who fear that penetration will be painful and also may stem from a sexual phobia or from a previous traumatic or painful experience.</li>

                                </ul>

                                <br><br>



                                <h3><strong>What causes sexual dysfunction?</strong></h3>

                                <div class="mid-bar"></div>

                                <p>Causes of sexual dysfunction include:</p>



                                <ul class="list2">

                                    <li><strong>Physical causes:</strong> Many physical and/or medical conditions can cause problems with sexual function. These conditions include diabetes, heart disease, neurological disorders, hormonal imbalances, menopause, chronic diseases such as kidney or liver failure, and alcoholism and drug abuse. In addition, the side effects of certain medications, including some antidepressant drugs, can affect sexual desire and function.</li>



                                    <li><strong>Psychological causes:</strong> These include work-related stress and anxiety, concern about sexual performance, marital or relationship problems, depression, feelings of guilt, and the effects of a past sexual trauma.</li>

                                </ul>

                              </div>

                        </div>

                    </div>

                </div>

            </div>



             <div class="col-xl-4 col-lg-4">
                    @include('pages/latest-news')
                    @include('pages/related-diseas')
                    @include('pages/quick-contact')
                </div>

        </div>

    </div>

    </div>



</main>

@endsection