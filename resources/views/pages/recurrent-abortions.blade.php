@extends('master.default')

@section('content')



<main>

    <!-- breadcrumb-area -->

    <section class="breadcrumb-area blue-bg">

        <div class="container">

            <div class="row">

                <div class="col-xl-6 col-md-6 mb-15 mt-15">

                    <div class="breadcrumb-title">

                        <h3>Recurrent Abortions</h3>

                    </div>

                </div>

                <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">

                    <div class="breadcrumb">

                        <ul>

                            <li><a href="#">Home</a></li>

                            <li><a href="#">Female Factor Infertility</a></li>

                            <li><a href="#">Recurrent Abortions</a></li>

                        </ul>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <!--Premeture Ejeculation Problems -->



    <div class="services-area pt-50 pb-65">

    <div class="container">

        <div class="row">

            <div class="col-xl-8 col-lg-8">

                <div class="row">

                    <div class="col-xl-12">

                        <div class="service-details mb-20">

                            <img class="mb-30" src="{!! asset('front/img/female-factor-infertility/abortion.jpg') !!}" alt="">

                            <br>

                            <h3>Recurrent Abortions ?</h3>

                            <div class="mid-bar"></div>

                            

                            </div>

                        </div>

                    </div>

                </div>

        

                 <div class="col-xl-4 col-lg-4">
                    @include('pages/latest-news')
                    @include('pages/related-diseas')
                    @include('pages/quick-contact')
                </div>

        </div>

    </div>

    </div>



</main>

@endsection