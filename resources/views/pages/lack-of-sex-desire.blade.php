@extends('master.default')

@section('content')



<main>

    <!-- breadcrumb-area -->

    <section class="breadcrumb-area blue-bg">

        <div class="container">

            <div class="row">

                <div class="col-xl-6 col-md-6 mb-15 mt-15">

                    <div class="breadcrumb-title">

                        <h3>Lack of Sex Desire</h3>

                    </div>

                </div>

                <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">

                    <div class="breadcrumb">

                        <ul>

                            <li><a href="#">Home</a></li>

                            <li><a href="#">Women's Sex Problems</a></li>

                            <li><a href="#">Lack of Sex Desire</a></li>

                        </ul>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <!--Premeture Ejeculation Problems -->



    <div class="services-area pt-50 pb-65">

    <div class="container">

        <div class="row">

            <div class="col-xl-8 col-lg-8">

                <div class="row">

                    <div class="col-xl-12">

                        <div class="service-details mb-20">

                            <img class="mb-30" src="{!! asset('front/img/women-problems/lack-of-desire-details.jpg') !!}" alt="">

                            <br>

                            <h3>Lack of Sex Desire ?</h3>

                            <div class="mid-bar"></div>

                            <p>A woman's sexual desires naturally fluctuate over the years. Highs and lows commonly coincide with the beginning or end of a relationship or with major life changes, such as pregnancy, menopause or illness. Some antidepressants and anti-seizure medications also can cause low sex drive in women.</p>

                            <p>If you have a persistent or recurrent lack of interest in sex that causes you personal distress, you may have hypoactive sexual desire disorder.</p>

                            <p>But you don't have to meet this medical definition to seek help. If you are bothered by a low sex drive or decreased sex drive, there are lifestyle changes and sex techniques that may put you in the mood more often. Some medications may offer promise as well.</p>

                            <br>

                            <h3>Symtoms:</h3>

                              <div class="mid-bar"></div>

                                <div class="service-details mt-10 mb-40">

                                <p>If you want to have sex less often than your partner does, neither one of you is necessarily outside the norm for people at your stage in life — although your differences may cause distress.</p>



                                <p>Similarly, even if your sex drive is weaker than it once was, your relationship may be stronger than ever. Bottom line: There is no magic number to define low sex drive. It varies from woman to woman.</p>



                                <p>Some signs and symptoms that may indicate a low sex drive include a woman who:</p>



                                <ul class="list2">

                                    <li>Has no interest in any type of sexual activity, including masturbation</li>

                                    <li>Doesn't have sexual fantasies or thoughts, or only seldom has them</li>

                                    <li>Is bothered by her lack of sexual activity or fantasies</li>

                                </ul>

                                <br><br>

                                <h3><strong>When to see a doctor:</strong></h3>

                                <div class="mid-bar"></div>

                                <p>If you're bothered by your low desire for sex, talk to your doctor. The solution could be as simple as changing the type of antidepressant you take.</p>



                                <h6><strong>Causes:</strong></h6>

                                <p>Woman’s desire for sex is based on a complex interaction of many components affecting intimacy, including physical well-being, emotional well-being, experiences, beliefs, lifestyle and current relationship. If you're experiencing a problem in any of these areas, it can affect your sexual desire.</p>

                                <br>



                                <h3><strong>Physical causes:</strong></h3>

                                <div class="mid-bar"></div>

                                <p>A wide range of illnesses, physical changes and medications can cause a low sex drive, including:</p>

                                 <ul class="list2">

                                    <li><strong>Sexual problems:</strong> If you experience pain during sex or an inability to orgasm, it can hamper your desire for sex.</li>

                                    <li><strong>Medical diseases:</strong> Numerous nonsexual diseases can also affect desire for sex, including arthritis, cancer, diabetes, high blood pressure, coronary artery disease and neurological diseases.</li>

                                    <li><strong>Medications:</strong> Many prescription medications — including some antidepressants and anti-seizure medications — are notorious libido killers.</li>

                                    <li><strong>Lifestyle habits:</strong> A glass of wine may make you feel amorous, but too much alcohol can spoil your sex drive; the same is true of street drugs. And smoking decreases blood flow, which may dampen arousal.</li>

                                    <li><strong>Surgery:</strong> Any surgery related to your breasts or your genital tract can affect your body image, sexual function and desire for sex.</li>

                                    <li><strong>Fatigue:</strong> Exhaustion from caring for young children or aging parents can contribute to low sex drive. Fatigue from illness or surgery also can play a role in a low sex drive.</li>

                                </ul>

                                <br><br>



                                <h3><strong>Hormone changes:</strong></h3>

                                <div class="mid-bar"></div>

                                <p>Changes in your hormone levels may alter your desire for sex. This can occur during:</p>



                                <ul class="list2">

                                    <li><strong>Menopause:</strong> Estrogen levels drop during the transition to menopause. This can cause decreased interest in sex and dryer vaginal tissues, resulting in painful or uncomfortable sex. At the same time, women may also experience a decrease in testosterone — a hormone that boosts sex drive in men and women alike — which may lead to decreased libido. Although many women continue to have satisfying sex during menopause and beyond, some women experience a lagging libido during this hormonal change.</li>



                                    <p>At the same time, women may also experience a decrease in testosterone — a hormone that boosts sex drive in men and women alike —which may lead to decreased libido. Although many women continue to have satisfying sex during menopause and beyond, some women experience a lagging libido during this hormonal change.</p>



                                    <li><strong>Pregnancy and breast-feeding:</strong> Hormone changes during pregnancy, just after having a baby and during breast-feeding can put a damper on sex drive. Of course, hormones aren't the only factor affecting intimacy during these times. Fatigue, changes in body image, and the pressures of pregnancy or caring for a new baby can all contribute to changes in your sexual desire.</li>

                                </ul>

                                <br><br>



                                <h3><strong>Psychological cause:</strong></h3>

                                <div class="mid-bar"></div>

                                <p>Your problems don't have to be physical or biological to be real. There are many psychological causes of low sex drive, including:</p>

                                <ul class="list2">

                                    <li>Mental health problems, such as anxiety or depression</li>

                                    <li>Stress, such as financial stress or work stress</li>

                                    <li>Poor body image</li>

                                    <li>Low self-esteem</li>

                                    <li>History of physical or sexual abuse</li>

                                    <li>Previous negative sexual experiences</li>

                                </ul>

                                <br><br>



                                <h3><strong>Relationship issues:</strong></h3>

                                <div class="mid-bar"></div>

                                <p>For many women, emotional closeness is an essential prelude to sexual intimacy. So problems in your relationship can be a major factor in low sex drive. Decreased interest in sex is often a result of ongoing issues, such as:</p>

                                <ul class="list2">

                                    <li>Lack of connection with your partner. </li>

                                    <li>Unresolved conflicts or fights. </li>

                                    <li>Poor communication of sexual needs and preferences </li>

                                    <li>Infidelity or breach of trust</li>

                                </ul>

                              </div>

                        </div>

                    </div>

                </div>

            </div>



             <div class="col-xl-4 col-lg-4">
                    @include('pages/latest-news')
                    @include('pages/related-diseas')
                    @include('pages/quick-contact')
                </div>

        </div>

    </div>

    </div>



</main>

@endsection