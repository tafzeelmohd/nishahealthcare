@extends('master.default')
@section('content')

<main>
    <!-- breadcrumb-area -->
    <section class="breadcrumb-area blue-bg">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-6 mb-15 mt-15">
                    <div class="breadcrumb-title">
                        <h3>Female Sex Problems</h3>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Service</a></li>
                            <li><a href="#">Female Sex Problems</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Male Sexual Problems -->

    <section class="related-project pt-50 pb-70">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 text-center pb-40">
                        <div class="section-title service-title">
                            <h2>Female Sexual Problems</h2>
                            <p><strong>Important:</strong> All Female Patients will be consulted by Dr. Ruby Siddiqui (Women's Infertility Specialist & Gynaecologist)</p>
                        </div>
                    </div>
                </div>
               <div class="row service-shadow">
                   <div class="col-xl-4 col-lg-4 col-md-12 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="{{'small-breasts'}}">
                                    <img src="{!! asset('front/img/women-problems/small-breast1.jpg') !!}" alt="">
                                </a>
                            </div>
                            <div class="link-box home-blog-link text-center">
                                <a href="{{'small-breasts'}}">Small Breast</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-12 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="{{'lack-of-sex-desire'}}">
                                    <img src="{!! asset('front/img/women-problems/absent-orgasm.jpg') !!}" alt="">
                                </a>
                            </div>
                            <div class="link-box home-blog-link text-center">
                                <a href="{{'lack-of-sex-desire'}}">Lack of Sex Desire</a>
                            </div>
                        </div>
                    </div>
                     <div class="col-xl-4 col-lg-4 col-md-12 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="{{'absent-orgasm'}}">
                                    <img src="{!! asset('front/img/women-problems/lack-of-sex.jpg') !!}" alt="">
                                </a>
                            </div>
                            <div class="link-box home-blog-link text-center">
                                <a href="{{'absent-orgasm'}}">Absent Orgasm</a>
                            </div>
                        </div>
                    </div>
                     <div class="col-xl-4 col-lg-4 col-md-12 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="{{'painfull-intercourse'}}">
                                   <img src="{!! asset('front/img/women-problems/painful-intercourse.jpg') !!}" alt="">
                                </a>
                            </div>
                            <div class="link-box home-blog-link text-center">
                                <a href="{{'painfull-intercourse'}}">Painfull Intercourse</a>
                            </div>
                        </div>
                    </div>
                     <div class="col-xl-4 col-lg-4 col-md-12 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="{{'women-infertility'}}">
                                     <img src="{!! asset('front/img/about/8.jpg') !!}" alt="">
                                </a>
                            </div>
                            <div class="link-box home-blog-link text-center">
                                <a href="{{'women-infertility'}}">Women Infertility</a>
                            </div>
                        </div>
                    </div>
                     <div class="col-xl-4 col-lg-4 col-md-12 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="{{'women-obesity'}}">
                                     <img src="{!! asset('front/img/women-problems/obesity-prob.jpg') !!}" alt="">
                                </a>
                            </div>
                            <div class="link-box home-blog-link text-center">
                                <a href="{{'women-obesity'}}">Obesity</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
</main>
@endsection