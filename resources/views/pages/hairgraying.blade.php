@extends('master.default')

@section('content')



<main>

    <!-- breadcrumb-area -->

    <section class="breadcrumb-area blue-bg">

        <div class="container">

            <div class="row">

                <div class="col-xl-6 col-md-6 mb-15 mt-15">

                    <div class="breadcrumb-title">

                        <h3>Hair Greying</h3>

                    </div>

                </div>

                <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">

                    <div class="breadcrumb">

                        <ul>

                            <li><a href="#">Home</a></li>

                            <li><a href="#">Common Diseases</a></li>

                            <li><a href="#">Hair Greying</a></li>

                        </ul>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <!--Premeture Ejeculation Problems -->



    <div class="services-area pt-50 pb-65">

    <div class="container">

        <div class="row">

            <div class="col-xl-8 col-lg-8">

                <div class="row">

                    <div class="col-xl-12">

                        <div class="service-details mb-20">

                            <img class="mb-30" src="{!! asset('front/img/common-problems/hairgray-detail.jpg') !!}" alt="">

                            <br>

                            <h3>Hair Greying ?</h3>

                            <div class="mid-bar"></div>

                            

                            <p>Hair is a filamentous biomaterial, that grows from follicles found in the dermis. Hair is one of the defining characteristics of mammals, but is also found in other animals.</p>



                            <p>The human body, apart from areas of glabrous skin, is covered in follicles which produce thick terminal and fine vellus hair. Most common interest in hair is focused on hair growth, hair types and hair care, but hair is also an important biomaterial primarily composed of protein, notably keratin. In many human societies, women predominantly grow the hair on their head long while men cut theirs short.</p>



                            <p>There is no scientific evidence that any diet, herb, supplement, or product can reduce or reverse gray hair, however, there are some underlying conditions that may result in premature gray and some ancient approaches you might be interested in.</p>



                            <p><strong>Conditions are given below - </strong></p>

                            

                            <p>Thyroid disorders. Conditions such as Grave's disease, Hashimoto's disease, hyperthyroidism, and hypothyroidism have been linked with premature gray hair.

                            Vitamin B12 deficiency anemia. It can be due to a diet low in vitamin B12, which is found mainly in meat, eggs, and milk. It can also occur if the stomach can't absorb vitamin B12, due to surgery involving the stomach or small intestine (such as gastric bypass surgery), diseases that affect the small intestine, such as Crohn's disease, celiac disease, fish tapeworms, or small intestine bacterial overgrowth, or a lack of protein called intrinsic factor due to an autoimmune reaction or a genetic defect.</p>

                            

                            <p>Vitiligo. A condition in which your skin loses melanocytes, resulting in very light patches of skin and possibly premature gray hair.</p>



                            <p>Early menopause

                            Smoking. Cigarette smoking has been linked to premature gray hair.

                            Addressing these underlying conditions, in some cases, may help to prevent hair from going premature gray. In traditional Chinese medicine (TCM) and Ayurveda, the traditional medicine of India, premature gray hair reflects an underlying disturbance. Hair, according to TCM, reflects the quality of blood and the strength of the kidneys. The kidneys and blood have a broader role in TCM than they do in Western physiology.

                            </p>



                            <p>Foods that are thought to strengthen the blood and kidneys are:</p>

                            <ul class="list2">

                            <li>Hijiki seaweed</li>

                            <li>Blackstrap molasses</li>

                            <li>Black sesame seeds</li>

                            <li>Nettles</li>

                            <li>Wheat grass</li>

                            <li>Chlorophyll</li></ul>

                            <br>

                            <p>Keep in mind though that consuming excessive amounts of these foods is not recommended. For example, excessive amounts of iodine (from seaweed) may increase the risk of developing thyroid problems, which itself is a cause of premature gray hair. Foods to avoid, according to TCM, are excessive meat, dairy, and salt</p>



                            </div>

                        </div>

                    </div>

                </div>

          



            <div class="col-xl-4 col-lg-4">
                    @include('pages/latest-news')
                    @include('pages/related-diseas')
                    @include('pages/quick-contact')
                </div>
        </div>

    </div>

    </div>



</main>

@endsection