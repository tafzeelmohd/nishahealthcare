@extends('master.default')

@section('content')



<main>

    <!-- breadcrumb-area -->

    <section class="breadcrumb-area blue-bg">

        <div class="container">

            <div class="row">

                <div class="col-xl-6 col-md-6 mb-15 mt-15">

                    <div class="breadcrumb-title">

                        <h3>Obesity</h3>

                    </div>

                </div>

                <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">

                    <div class="breadcrumb">

                        <ul>

                            <li><a href="#">Home</a></li>

                            <li><a href="#">Common Diseases</a></li>

                            <li><a href="#">Obesity</a></li>

                        </ul>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <!--Premeture Ejeculation Problems -->



    <div class="services-area pt-50 pb-65">

    <div class="container">

        <div class="row">

            <div class="col-xl-8 col-lg-8">

                <div class="row">

                    <div class="col-xl-12">

                        <div class="service-details mb-20">

                            <img class="mb-30" src="{!! asset('front/img/common-problems/weight-gain-deatil.jpg') !!}" alt="">

                            <br>

                            <h3>Obesity ?</h3>

                            <div class="mid-bar"></div>

                            

                            <p>Being extremely obese means you are especially likely to have health problems related to your weight. The good news is that even modest weight loss can improve or prevent the health problems associated with obesity. Dietary changes, increased physical activity and behavior changes can help you lose weight. Prescription medications and weight-loss surgery are additional options for treating obesity. Obesity is a complex disorder involving an excessive amount of body fat. Obesity isn't just a cosmetic concern. It increases your risk of diseases and health problems, such as heart disease, diabetes and high blood pressure.</p>



                            <p><strong>What Are the Signs and Symptoms of Overweight and Obesity?</strong></p>



                            <p>Weight gain usually happens over time. Most people know when they've gained weight. Some of the signs of overweight or obesity include:</p>



                            <ul class="list2">

                                <li>Clothes feeling tight and needing a larger size.</li>

                                <li>The scale showing that you've gained weight.</li>

                                <li>The scale showing that you've gained weight.</li>

                                <li>A higher than normal body mass index and waist circumference.</li>

                            </ul>



                            <p><strong>When to see a doctor </strong></p>

                            

                            <p>If you think you may be obese, and especially if you're concerned about weight-related health problems, see your doctor or health care provider. You and your provider can evaluate your health risks and discuss your weight-loss options.</p>

                            

                            <p><strong>Risk factors</strong></p>

                            <p>Obesity usually results from a combination of causes and contributing factors, including:</p>

                            <ul class="list2">

                                <li>Genetics : Your genes may affect the amount of body fat you store, and where that fat is distributed. Genetics may also play a role in how efficiently your body converts food into energy and how your body burns calories during exercise.</li>

                                <li>Family lifestyle : Obesity tends to run in families. If one or both of your parents are obese, your risk of being obese is increased. That's not just because of genetics. Family members tend to share similar eating and activity habits.</li>

                                <li>Inactivity : If you're not very active, you don't burn as many calories. With a sedentary lifestyle, you can easily take in more calories every day than you burn through exercise and routine daily activities. Having medical problems, such as arthritis, can lead to decreased activity, which contributes to weight gain.</li>

                                <li>Unhealthy diet : A diet that's high in calories, lacking in fruits and vegetables, full of fast food, and laden with high-calorie beverages and oversized portions contributes to weight gain.</li>

                                <li>Medical problems : In some people, obesity can be traced to a medical cause, such as Prader-Willi syndrome, Cushing's syndrome and other conditions. Medical problems, such as arthritis, also can lead to decreased activity, which may result in weight gain.</li>

                                <li>Certain medication : Some medications can lead to weight gain if you don't compensate through diet or activity. These medications include some antidepressants, anti-seizure medications, diabetes medications, antipsychotic medications, steroids and beta blockers.</li>

                                <li>Social and economic issues : Research has linked social and economic factors to obesity. Avoiding obesity is difficult if you don't have safe areas to exercise. Similarly, you may not have been taught healthy ways of cooking, or you may not have money to buy healthier foods. In addition, the people you spend time with may influence your weight — you're more likely to become obese if you have obese friends or relatives.</li>

                                <li>Age : Obesity can occur at any age, even in young children. But as you age, hormonal changes and a less active lifestyle increase your risk of obesity. In addition, the amount of muscle in your body tends to decrease with age. This lower muscle mass leads to a decrease in metabolism. These changes also reduce calorie needs, and can make it harder to keep off excess weight. If you don't consciously control what you eat and become more physically active as you age, you'll likely gain weight.</li>

                                <li>Pregnancy : During pregnancy, a woman's weight necessarily increases. Some women find this weight difficult to lose after the baby is born. This weight gain may contribute to the development of obesity in women.</li>

                                <li>Quitting smoking : Quitting smoking is often associated with weight gain. And for some, it can lead to enough weight gain that the person becomes obese. In the long run, however, quitting smoking is still a greater benefit to your health than continuing to smoke.</li>

                                <li>Lack of sleep. : Not getting enough sleep or getting too much sleep can cause changes in hormones that increase your appetite. You may also crave foods high in calories and carbohydrates, which can contribute to weight gain.</li>

                            </ul>

                            <p>Even if you have one or more of these risk factors, it doesn't mean that you're destined to become obese. You can counteract most risk factors through diet, physical activity and exercise, and behaviour changes.</p>



                            <p><strong>Other weight-related issues that may affect your quality of life include:</strong></p>

                             <ul class="list2">

                                <li>Depression</li>

                                <li>Disability</li>

                                <li>Sexual problems</li>

                                <li>Shame and guilt</li>

                                <li>Social isolation</li>

                                <li>Lower work achievement</li>

                            </ul>



                            <p><strong>Symptoms and Complications of Obesity</strong></p>

                            <ul class="list2">

                                <li>breathing disorders (e.g., sleep apnea, chronic obstructive pulmonary disease)</li>

                                <li>certain types of cancers (e.g., prostate and bowel cancer in men, breast and uterine cancer in women)</li>

                                <li>coronary artery (heart) disease</li>

                                <li>depression</li>

                                <li>diabetes</li>

                                <li>gallbladder or liver disease</li>

                               <li>gastroesophageal reflux disease (GERD)</li>

                                <li>high blood pressure</li>

                                <li>high cholesterol</li>

                                <li>joint disease (e.g., osteoarthritis)</li>

                                <li>stroke</li>

                            </ul>

                            </div>

                        </div>

                    </div>

                </div>

          



            <div class="col-xl-4 col-lg-4">
                    @include('pages/latest-news')
                    @include('pages/related-diseas')
                    @include('pages/quick-contact')
                </div>

        </div>

    </div>

    </div>



</main>

@endsection