@extends('master.default')
@section('content')

<main>
        <!-- breadcrumb-area -->
        <section class="breadcrumb-area blue-bg">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-md-6 mb-15 mt-15">
                        <div class="breadcrumb-title">
                            <h3>Infrastructure</h3>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">
                        <div class="breadcrumb">
                            <ul>
                                <li><a href="#">Home</a></li>
                                <li><a href="#">About</a></li>
                                <li><a href="#" class="active">Infrastructure</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- about-info -->

        <!-- about-area -->
            <section class="portfolio-area grey-bg pt-40 pb-40">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="about-info-text">
                            <h2>Infrastructure and Facilities</h2>
                            <p>Linear Shock Wave Therapy (LSWT) is a new noninvasive therapy that uses low-intensity shock waves to induce local angiogenesis promising modality in the treatment of erectile dysfunction (ED).</p>
                    
                            <div class="info-img">
                               <video width="100%" height="530" controls autoplay preload = "auto">
                                  <source src="{!! asset('front/video/vid.mp4') !!}" type="video/mp4">
                               </video>
                            </div>
                            <div class="text-left para">
                            <br><br>
                            <p><strong>OBJECTIVE:</strong><p>
                            <p>To evaluate the effectiveness of LSWT in men with vasculogenic erectile dysfunction (ED), in a Nisha Health Care Center.</p>

                            <p><strong>MATERIAL AND METHODS:</strong></p>
                            <p>Included 15 men aged 45-70 years, sexually active with mild and moderate vascular ED evaluated with the International Index of Erectile Function (IIEF). The study was conducted in three stage: screening, treatment and results. Treatment stage: 4 weekly sessions LSWT (RENOVA ®) 5000 waves (.09mJ/mm(2)). Erectile function was assessed with IIEFF-EF, SEP (Sexual Encounter Profile) and GAQ (Global Assessment Questions) at one and six months after treatment.</p>

                            <p><strong>RESULTS:</strong></p>
                            <p>
                            The rate of success was 80% (12/15). Patients with mild ED (6/15) 40% and moderate ED (9/15) 60%. We found a positive association between IIEF-Basal (average 14.23 pts) and IIEF at one month and six months after therapy (19.69 pts) a difference of 5.46 pts.</p>

                            <p><strong>CONCLUSIONS:</strong></p>
                            <p>The feasibility and tolerability of this treatment, and rehabilitation potential features, make it this an attractive new treatment option for patients with ED.</p>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
       </section>
    </main>
    <style>
        .para p
        {
            padding:0;
            font-size: 13px;
            margin: 7px 0;
        }
        video {
    object-fit: fill;
}
    </style>
@endsection