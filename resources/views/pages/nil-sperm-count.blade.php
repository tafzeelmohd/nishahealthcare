@extends('master.default')

@section('content')



<main>

    <!-- breadcrumb-area -->

    <section class="breadcrumb-area blue-bg">

        <div class="container">

            <div class="row">

                <div class="col-xl-6 col-md-6 mb-15 mt-15">

                    <div class="breadcrumb-title">

                        <h3>Low Sperm Count</h3>

                    </div>

                </div>

                <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">

                    <div class="breadcrumb">

                        <ul>

                            <li><a href="#">Home</a></li>

                            <li><a href="#">Male Factor Infertility</a></li>

                            <li><a href="#">Nil Sperm Count</a></li>

                        </ul>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <!--Premeture Ejeculation Problems -->



    <div class="services-area pt-50 pb-65">

    <div class="container">

        <div class="row">

            <div class="col-xl-8 col-lg-8">

                <div class="row">

                    <div class="col-xl-12">

                        <div class="service-details mb-20">

                            <img class="mb-30" src="{!! asset('front/img/male-factor-infertility/nil-sperm-count.jpg') !!}" alt="">

                            <br>

                            <h3>Nil Sperm Count ?</h3>

                            <div class="mid-bar"></div>

                            <p><strong>What is Azoospermia (Nil Sperm):</strong>  Azoospermia is called when there is no sperm in semen. This type of semen disorder is found in approximately 3% of infertile men i.e. absent sperm. You should know that testis has two separate functions

                            <p>(i) Production of normal sperms in semen which needed for pregnancy & normal fertility.</p>

                            <p>(ii)The other function of testis is production of male hormones i.e. testosterone & others. So in most patients with nil sperms though semen has absent sperms still production of male hormones remains normal.</p>

                            To conceive a child, a male's sperm must combine with a female's egg. The testicles make and store sperm, which are ejaculated by the penis to deliver sperm to the female reproductive tract during sexual intercourse.

                            The most common issues that lead to infertility in men are problems that affect how the testicles work. Other problems are hormone imbalances or blockages in the male reproductive organs. In about 50% of cases, the cause of male infertility cannot be determined.

                            A complete lack of sperm is the cause of infertility in about 10% to 15% of men who are infertile. When a man does not produce sperm, it is called azoospermia . A hormone imbalance or a blockage of sperm movement can cause azoospermia.

                            In some cases of infertility, a man produces less sperm than normal. This condition is called oligospermia. The most common cause of oligospermia is varicocele, an enlarged vein in the testicle.</p>

                           

                            <br>

                            <h3>Azoospermia: </h3>

                              <div class="mid-bar"></div>

                                <div class="service-details mt-10 mb-40">

                                <p>Azoospermia is the medical condition of a man not having any measurable level of sperm in his semen. It is associated with very low levels of fertility or even sterility, but many forms are amenable to medical treatment. In humans, azoospermia affects about 1% of the male population and may be seen in up to 20% of male infertility situations. If this is the case, then one or both of two conditions may be present-</p>



                                <p>* There is problem with sperm production.</p>

                                <p>* There is a blockage such that sperm production, although normal, cannot reach the ejaculate</p>



                                <h3><strong>How sperms develop: </strong></h3>

                                <div class="mid-bar"></div>

                                <p> When boy becomes of 14 years of age then L.H. & F.S.H. hormone secretion from pituitary increases. The rise in these hormones leads to proliferation of sperm forming cells (Germ Cells) in the testis. These germ cells start multiplying under the effect of above-mentioned pituitary hormones along with assistance of other hormones as testosterones, Growth hormones, Androstenidione, insulin like growth factor-I, Thyroids hormone, paracrine hormone & growth factors. Under the control of above-mentioned hormones germs cells divide & transformed into primary spermatocytes. Then further maturation of primary spermatocytes to spermatids & then finally into mature spermatozoa (i.e. normal sperms) occurs under the control of above-mentioned hormones. After few weeks of progressive maturation inside the testis these sperms become normally motile & develop the capacity to fertilize the ovum. This total sperm cycle, from first stage to final stage of normal mature sperms is of three months. Thus to produce normal sperms testis should have normal sperm producing germ cells & normal regulating hormones. Any major hindrance in the development of these spermatozoa will lead to absent sperm production resulting into nil sperm.</p>



                                <p><strong>Causes  of Nil Sperms:</strong> The various causes of  nil sperm are as follows: The various causes of low sperm count are as follows:</p>

                                <p><strong>Hormone disorder:</strong> The various endocrine (Hormone) disorder leading to azoospermia are as follows.</p>



                                <p>(i) Hormone deficiency</p> of pituitary gland as L.H., F.S.H., Prolactin, thyroids hormone, hypothalmic deficiency of GnRH, Pituitary gland failure, Hypopituitarism, Idiopathic hypopituitarism, Kallman syndrome, Isolated hypogonadotropic hypogonadism, Drugs, toxins, Idiopathic hypogonadotropic hypogonadism & due to many more causes.</p>



                                <p><strong>(ii) Obstruction</strong> in the outflow of semen (Sperms) from testis to outside through urethral opening. Many times the production of sperms in testis is absolutely normal but these sperm are unable to come out due to obstruction in the out flow tract leading to absent sperms in the semen. The various causes of obstruction are absent vas deferens, absent seminal vesicle, posttraumatic, post surgical ligation of vas deferens. After some infections, as chlamydial, gonococcal urethritis. It may also be due to post tubercular epididimo-orchitis. The sperm may also not come out of testis if the are imotile due to any of the following causes as imotile cilia syndrome, kartagener syndrome cystic fibrosis & many other rare diseases.</p>

                                <p><strong>(iii) Absence of germ cells</strong> in testis also called sertoli cell only syndrome. In this there are no germs cells i.e. sperm forming cells in the testis. For you knowledge, I wish to inform you that in testis germ cell come to testis from neural cord area of the body during neural cord area of the body during development of fetus. So in some fetuses this migration of sperm cells do not occurs leading to testis only having testosterone forming & sertoli cells. Thus this condition is called sertoli sell only syndrome it is a developmental defect.</p>

                                <p><strong>(v) Maturation Arrest (. Spermatid arrest):</strong> of primary spermatocytes to secondary spermatocyte, spermatids or to mature spermatozoa. Due to may local, systemic, hormonal growth factor deficiency or due to idiopathic factor. The various paracrine hormones and growth factors are essential for normal development i.e. maturation of one germ cells to multiplication of ultimately production of multiple mature, normal & motile sperms. Many other factor as infection, varicocele, drugs, chemotherapy may also lead to maturation arrest. The other causes may by developmentally defective germs cells & spermatocyte. So that they did not have inherent capacity of developing into a mature & motile sperms.</p>

                                <p><strong>(vi) Testicular disorders</strong> (primary leydig cell dysfunction), Chromosomal (Klinefelter syndrome and variants, XX male gonadal dysgenesis), Defects in androgen biosynthesis, Orchitis (mumps, HIV, other viral, ),Myotonia dystrophica, Toxins (alcohol, opiates, fungicides, insecticides, heavy metals, cotton seed oil), Drugs (cytotoxic drugs, ketoconazole, cimetidine, spironolactone)</p>

                                <p><strong>vii) aricocele (Grade 3 or more severe):</strong> A varicocele is a varicose vein in the cord that connects to the testicle. (A varicose vein is one that is abnormally enlarged and twisted.)

                                Varicocele decreases sperm productions by elevating temperature of the testis, may produce higher levels of nitric oxide chemical in the testis which blocks sperm production, varicocele damages sperms directly & lastly varicocele decrease the oxygen supply to testis.</p>

                              </div>

                        </div>

                    </div>

                </div>

            </div>



            <div class="col-xl-4 col-lg-4">
                    @include('pages/latest-news')
                    @include('pages/related-diseas')
                    @include('pages/quick-contact')
                </div>

        </div>

    </div>

    </div>



</main>

@endsection