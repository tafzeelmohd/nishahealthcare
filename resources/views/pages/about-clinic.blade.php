@extends('master.default')

@section('content')



<main>

        <!-- breadcrumb-area -->

        <section class="breadcrumb-area blue-bg">

            <div class="container">

                <div class="row">

                    <div class="col-xl-6 col-md-6 mb-15 mt-15">

                        <div class="breadcrumb-title">

                            <h3>About Our Clinic</h3>

                        </div>

                    </div>

                    <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">

                        <div class="breadcrumb">

                            <ul>

                                <li><a href="#">Home</a></li>

                                <li><a href="#">About</a></li>

                                <li><a href="#" class="active">Clinic</a></li>

                            </ul>

                        </div>

                    </div>

                </div>

            </div>

        </section>

        <!-- about-info -->



        <!-- about-area -->

     <div class="services-area pt-70 pb-65">

            <div class="container">

                <div class="row">

                    <div class="col-12 mb-45">

                        <div class="details-thumb">

                            <img src="{!! asset('front/img/infrastructure/B612_20181212_194046.jpg') !!}" class="img-fluid" alt="">

                        </div>

                    </div>

                </div>

                <div class="row">



                    <div class="col-xl-12 col-lg-12 mb-30">

                        <div class="project-desc">

                            <h3>About Our Clinic</h3><div class="mid-bar"></div>

                            <p>Dr. M. M. Siddiqui is one of the best Sexologist in the in Uttar Pradesh India according to various surveys and awards achieved by National and International Ministry.</p>



                            <p>Being one of the most famous clinics in India’s various states like Delhi and Uttar Pradesh providing complete analysis and treatment facility to patients for various male & female sex problems or disorders.</p>



                            <p>As we realize that there are very few trained sexologists in India that is why all patients with sexual disorders are exploited by unqualified quacks prescribing Allopathic, Unani or Herbal medicines at exorbitant prices. There is hardly any clinic in India which offers proper diagnosis & scientific treatment of sexual problems or disorders. Most of the times patient does not know whom to consult about his problems and these are the reasons for sexual diseases to become a taboo in India.</p>



                            <p>Nisha health Care Ltd. is opened with the mission to provide all the facility for diagnosis & treatment by modern medication system i.e. Allopathic & Ayurveda. At our clinic, we have a team of highly qualified & experienced male & female doctors who are specialized in Ayurveda & Allopathic for curing various sexual problems. It is the only clinic present in India and other countries which is treating its patient by Ayurveda & Allopathic to eradicate completely from its root cause for life time. First we diagnose the cause of sexual problems by detailed sex counseling, physical examination & relevant laboratory tests. Then we provide the treatment for the particular case, which we detect in investigation. We provide cure for almost all cases because our approach is scientific.Till now, we have cured approximately more than 2, 20,000 patients with different kind of sexual problems. All the medicines are provided to the patient from the clinic. The duration of treatment for most of the sexual problem is not more than one or two months.</p>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </main>

@endsection