@extends('master.default')

@section('content')



<main>

    <!-- breadcrumb-area -->

    <section class="breadcrumb-area blue-bg">

        <div class="container">

            <div class="row">

                <div class="col-xl-6 col-md-6 mb-15 mt-15">

                    <div class="breadcrumb-title">

                        <h3>Pimples</h3>

                    </div>

                </div>

                <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">

                    <div class="breadcrumb">

                        <ul>

                            <li><a href="#">Home</a></li>

                            <li><a href="#">Common Diseases</a></li>

                            <li><a href="#">Pimples</a></li>

                        </ul>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <!--Premeture Ejeculation Problems -->



    <div class="services-area pt-50 pb-65">

    <div class="container">

        <div class="row">

            <div class="col-xl-8 col-lg-8">

                <div class="row">

                    <div class="col-xl-12">

                        <div class="service-details mb-20">

                            <img class="mb-30" src="{!! asset('front/img/common-problems/pimples-detail.jpg') !!}" alt="">

                            <br>

                            <h3>Pimples ?</h3>

                            <div class="mid-bar"></div>

                            

                            <p>A pimple is a small pustule or papule. Pimples are small skin lesions or inflammations of the skin - they are sebaceous glands (oil glands) which are infected with bacteria, swell up, and then fill up with pus.Pimples are also known as spots or zits Pimples occur when the sebaceous glands, which are located at the base of hair follicles, become overactive. The most vulnerable parts of the body are the face, back, chest and shoulders. Pimples are palpable signs ofacne, especially when a breakout occurs. </p>



                            <p>Concern is growing among experts regarding the long-term use of antibiotics for acne treatment, and its contribution to bacterial resistance. Experts from the Centre of Evidence-Based Dermatology at the University of Nottingham, England, wrote in The Lancet (August 2011 issue) that although pharmacies are well stocked with a wide range of acne medications, few studies have been carried out regarding their efficacy.</p>



                            <p><strong>Causes of pimples / zits</strong></p>

                            

                            <p>The sebaceous glands, which produce sebum, exist inside the pores of our skin. The outer layers of our skin are being shed continuously. Sometimes, dead skin cells are left behind and get stuck together by the sticky sebum, causing a blockage in the pore. Pore blockage is more likely to occur during puberty (the process of physical changes by which a child's body becomes an adult body capable of reproduction). More sebum is produced by the sebaceous gland - as the pore is blocked, it accumulates behind the blockage.</p>



                            <p>This accumulated and blocked sebum has bacteria, including Propionibacterium acnes; this slow-growing bacterium is linked to acne. Propionibacterium acnes generally exists harmlessly on our skin - however, when the conditions are right, it can reproduce more rapidly and become a problem. The bacterium feeds off the sebum and produces a substance that causes an immune response, leading to inflammation of the skin and spots.</p>

                            

                            <p><strong>"Good" and "bad" bacteria determine severity and frequency of pimples</strong> - researchers at the Washington University School of Medicine identified two unique strains of P. acnes among 20% of people with pimples and hardly any among those with healthy skin. They found the opposite with a different P. acnes strain.</p>



                            <p>The skin of people who are prone to acne are especially sensitive to normal blood levels of testosterone - a natural hormone found in both males and females. In such people the testosterone can make the sebaceous glands produce too much sebum, making the clogging up of dead skin cells more likely, which in turn increases the probability of blocking the pores, etc.</p>



                            <p>You cannot catch pimples from another person; pimples are not infectious.</p>



                            <p><strong>Dairy products and high glycemic index foods linked to pimples </strong>- researchers from New York University reported in theJournal of the Academy of Nutrition and Dietetics that people who eat a lot of high glycemic index foods and dairy products are more likely to have acne. They also suggested that using medical nutrition therapy may help in the treatment of acne.</p>



                            <p><strong>Having pimples or acne can be hereditary.</strong></p>

                            <br>

                            <h3>Symptoms of pimples</h3>

                            <div class="mid-bar"></div>



                            <p>There are several different types of pimples and they have different signs and symptoms:</p>

                            <ul class="list2">

                                <li><strong>Whiteheads </strong>- also known as a closed comedo. These are very small and remain under the skin, appearing as a small, flesh-colored papules.</li>

                                <li><strong>Blackheads </strong>- also known as an open comedo. These are clearly visible; they are black and appear on the surface of the skin. Some people mistakenly believe they are caused by dirt, because of their color, and scrub their faces vigorously - this does not help and may irritate the skin and cause other problems.</li>

                                <li><strong>Papules </strong>- these are small, solid, rounded bumps that rise from the skin. The bumps are often pink.</li>

                                <li><strong>Pustules </strong>- these are pimples full of pus. They are clearly visible on the surface of the skin. The base is red and the pus is on the top.</li>

                                <li><strong>Nodules </strong>- these are morphologically similar (similar structure) to papules, but larger. They can be painful and are embedded deep in the skin.</li>

                                <li><strong>Cysts </strong>- these are clearly visible on the surface of the skin. They are filled with pus and are usually painful. Cysts commonly cause scars</li>

                            </ul>

                            <br>

                            <h3>How common are pimples (acne)</h3>

                            <div class="mid-bar"></div>



                            <p>Acne is the most common skin disease for adolescents. According to the British Medical Journal (Clinical Evidence, Authors: Sarah Purdy, David DeBerker):</p>



                            <ul class="list2">

                                <li>More than 80% of teenagers get acne at some point.</li>

                                <li>A community sample of 14 to 16 year-olds in the United Kingdom revealed that acne affected 50% of them.</li>

                                <li>A sample study of adolescents in New Zealand found acne was present in 91% of boys and 79% of girls.</li>

                                <li>A sample study of adolescents in Portugal found that the average prevalence of acne (in both sexes) was 82%.</li>

                                <li>30% of teenagers with acne required medical treatment because of its severity.</li>

                                <li>General practitioners (GPs, primary care physicians) in the UK reported that 3.1% of 13 to 25 year-old patients visited them complaining of acne.</li>

                                <li>The incidence of acne is similar in both adult males and females.</li>

                                <li>Doctors report that acne appears to peak at 17 years of age.</li>

                                <li>Acne incidence (presence, occurrence) in adults is increasing, doctors report. We don't know why.</li>

                            </ul>

                        

                            </div>

                        </div>

                    </div>

                </div>

          



            <div class="col-xl-4 col-lg-4">
                    @include('pages/latest-news')
                    @include('pages/related-diseas')
                    @include('pages/quick-contact')
                </div>

        </div>

    </div>

    </div>



</main>

@endsection