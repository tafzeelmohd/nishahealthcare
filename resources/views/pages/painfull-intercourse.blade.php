@extends('master.default')

@section('content')



<main>

    <!-- breadcrumb-area -->

    <section class="breadcrumb-area blue-bg">

        <div class="container">

            <div class="row">

                <div class="col-xl-6 col-md-6 mb-15 mt-15">

                    <div class="breadcrumb-title">

                        <h3>Painfull Intercourse</h3>

                    </div>

                </div>

                <div class="col-xl-6 col-md-6 text-left text-md-right mt-15">

                    <div class="breadcrumb">

                        <ul>

                            <li><a href="#">Home</a></li>

                            <li><a href="#">Women's Sex Problems</a></li>

                            <li><a href="#">Painfull Intercourse</a></li>

                        </ul>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <!--Painfull Intercourse Problems -->



    <div class="services-area pt-50 pb-65">

    <div class="container">

        <div class="row">

            <div class="col-xl-8 col-lg-8">

                <div class="row">

                    <div class="col-xl-12">

                        <div class="service-details mb-20">

                            <img class="mb-30" src="{!! asset('front/img/women-problems/painfull-intercourse-details.png') !!}" alt="">

                            <br>

                            <h3>PAINFUL INTER COURSE - VAGINISMUS ?</h3>

                            <div class="mid-bar"></div>

                            <p>When a woman has vaginismus, her vagina's muscles squeeze or spasm when something is entering her, like a tampon or a penis. It can be mildly uncomfortable, or it can be painful. There are exercises a woman can do that can help, sometimes within weeks.</p>

                            <br>



                            <h3>Symptoms?</h3>

                              <div class="mid-bar"></div>

                                <div class="service-details mt-10 mb-40">

                                <p>Painful sex is often a woman's first sign that she has vaginismus. The pain happens only with penetration. It usually goes away after withdrawal, but not always.</p>



                                <p>Women have described the pain as a tearing sensation or a feeling like the man is "hitting a wall.</p>



                                <p>Many women who have vaginismus also feel discomfort when inserting a tampon or during a doctor's internal pelvic exam.</p>



                                <br>

                                <h3><strong>Causes:</strong></h3>

                                <div class="mid-bar"></div>

                                <p>Doctors don't know exactly why vaginismus happens. It's usually linked to anxiety and fear of having sex. But it's unclear which came first, the vaginismus or the anxiety.</p>

                                <p>Some women have vaginismus in all situations and with any object. Others have it only in certain circumstances, like with one partner but not others, or only with sexual intercourse but not with tampons or during medical exams.</p>

                                <p>Other medical problems like infections can also cause painful intercourse. So it's important to see a doctor to determine the underlying cause of pain during sex.</p>



                                <h3><strong>Treatment:</strong></h3>

                                <div class="mid-bar"></div>

                                <p>Women with vaginismus can do exercises, in the privacy of their own home, to learn to control and relax the muscles around the vagina.</p>

                                <p><strong>For expert advice Contact Us:</strong></p>



                              </div>

                        </div>

                    </div>

                </div>

            </div>


                 <div class="col-xl-4 col-lg-4">
                    @include('pages/latest-news')
                    @include('pages/related-diseas')
                    @include('pages/quick-contact')
                </div>


        </div>

    </div>

    </div>



</main>

@endsection