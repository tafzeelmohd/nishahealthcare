@extends('master.default')
@section('content')

<style>
	video {
    height: 100% !important;
}.breadcrumb-title h2 {
   font-size: 35px;
    color: #fff;
    padding-left: 0;
}.breadcrumb-title p {
    color: #fff;
    font-size: 16px;
    font-style: italic;
}
.blog-wrapper
{
	padding-top: 0;
}
</style>

<main>

<section class="breadcrumb-area pb-70 pt-100 grey-bg" style="background-image:url(http://drmssiddiqui.com/public/front/img/about/marriage-banner.jpg)">
<div class="container">
<div class="row">
<div class="col-xl-6 col-md-6 mb-30">
<div class="breadcrumb-title sm-size">
<h2>Suhagraat Syndrome</h2>
<p>Do You have a Fear of Suhagraat?</p>
</div>
</div>
<div class="col-xl-6 col-md-6 text-left text-md-right mb-30">
<div class="breadcrumb">
<ul>
<li><a href="http://drmssiddiqui.com">Home</a></li>
<li><a href="#">Services</a></li>
<li><a href="#">Suhagraat Syndrome</a></li>
</ul>
</div>
</div>
</div>
</div>
</section>

<div class="basic-blog-area gray-bg pt-100 pb-40">
<div class="container">
<div class="row">
<div class="col-lg-8 blog-post-items">
<div class="blog-wrapper mb-60">
<div class="blog-thumb">
<!-- <img src="img/blog/l-01.jpg" alt=""> -->
<video width="100%" height="530" controls autoplay preload = "auto">
<source src="http://drmssiddiqui.com/public/front/video/marr.mp4" type="video/mp4" height="100%" autoplay>
</video>
</div>

<div class="blog-content">

<h2 class="blog-title">Suhagraat Syndrome ?</h2>

<p>Fear of sex or sexual intimacy is also called “genophobia” or “erotophobia.” This is more than a simple dislike or aversion. It’s a condition that can cause intense fear or panic when sexual intimacy is attempted. For some people, even thinking about it can cause these feelings.
There are other phobias related to genophobia that might occur at the same time:
nosophobia: fear of getting a disease or virus
gymnophobia: fear of nudity (seeing others naked, being seen naked, or both)
heterophobia: fear of the opposite sex
coitophobia: fear of intercourse
haphephobia: fear of being touched as well as touching others
tocophobia: fear of pregnancy or childbirth
A person might also have general fear or anxiety about being emotionally close with another person. This can then translate into a fear of sexual intimacy.. </p>

<h5 class="green-head">Causes of Suhagraat Syndrome:</h5>

<p><strong>Masturbation or depression : </strong></p>

<p><strong>Erectile dysfunction.</strong>Erectile dysfunction (ED) is difficulty obtaining and sustaining an erection. Although it’s treatable, it might lead to feelings of embarrassment, shame, or stress. Someone with ED might not want to share this with another person. Depending on how intense the feelings are, this might cause a person to become fearful of sexual intimacy.</p>

<p><strong>Vaginismus.</strong> Vaginismus is when the muscles of the vagina clench up involuntarily when vaginal penetration is attempted. This can make intercourse painful or even impossible. It can also interfere with inserting a tampon. Such severe and consistent pain can lead to a fear of sexual intimacy.

<p><strong>Past sexual abuse or PTSD.</strong> Child abuse or sexual abuse can cause post-traumatic stress disorder (PTSD) and affect the way you view intimacy or sex. It can also affect sexual functioning. While not every survivor of abuse develops PTSD or a fear of sex or intimacy, these things might be a part of some individuals’ fear of sex.

<p><strong>Fear of sexual performance.</strong> Some people are nervous about whether they’re “good” in bed. This can cause intense psychological discomfort, leading them to avoid sexual intimacy altogether for fear of ridicule or poor performance.

<p>Body shame or dysmorphia. Shame of one’s body, as well as being overly self-conscious about the body, can negatively impact sexual satisfaction and cause anxiety. Some individuals with severe body shame or dysmorphia (seeing the body as flawed although, to other people, it looks normal) may avoid or fear sexual intimacy altogether because of the lack of pleasure and intense shame it brings them.

<p>A history of rape. Rape or sexual assault can cause PTSD and various kinds of sexual dysfunction, including negative associations with sex. This might cause someone to develop a fear of sexual intimacy.</p>

<h5 class="green-head">Treatments:</h5>

<blockquote> निशा हेल्थ केयर में सभी सेक्स सम्बन्धी समस्यायों का समाधान किया जाता है| डॉक्टर सिद्दीकी से संपर्क करने के लिए आप निशा हेल्थ केयर में मिल सकते है | 
आप हमे मोबाइल नंबर 9289911911 पर कॉल कर सकते है | आप हमे nishahealthcare0786@gmail.com पर मेल भी लिख सकते है | 
आपकी सुविधा क लिए एक ऑनलाइन फॉर्म दिया गया है जिसे भर कर आप लाल रंग का "Submit" बटन क्लिक कर सकते है और हम आपको स्वंय कॉल करेंगे |
धन्यवाद !! </blockquote>
</div>
</div>
</div>


<div class="col-xl-4 col-lg-4">
@include('pages/latest-news')
@include('pages/quick-contact')
</div>

</div>
</div>
</div>
</main>



@endsection