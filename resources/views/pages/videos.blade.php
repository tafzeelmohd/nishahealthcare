@extends('master.default')
@section('content')

<main>
    <!-- breadcrumb-area -->
    <section class="breadcrumb-area blue-bg">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-md-4 mb-15 mt-15">
                    <div class="breadcrumb-title">
                        <h3>Videos</h3>
                    </div>
                </div>
                <div class="col-xl-8 col-md-8 text-left text-md-right mt-15">
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="{{'home'}}">Home</a></li>
                            <li><a href="javascript:;">Gallary</a></li>
                            <li><a class="active" href="{{'certificates'}}">Videos</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--Videos -->
       <section class="portfolio-area grey-bg pt-50 pb-40">
            <div class="container">
                  <br>
                <div class="row">
                    <div class="col-xl-12">
                        <div class="col-xl-12 text-center">
                            <div class="section-title service-title">
                                <h2>Videos</h2>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
    
                <div class="row portfolio-active">
                    
                    <div class="col-xl-4 col-lg-4 col-md-4 mb-30 grid-item">
                       <div class="video-play">
                        <img src="http://i3.ytimg.com/vi/VQaLN0_qylw/maxresdefault.jpg" class="img-fluid" style="width: 100%;">
                            <div class="call-to-action">
                                <a href="https://www.youtube.com/watch?v=VQaLN0_qylw" class="view video-view"><i class="fa fa-play"></i></a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-xl-4 col-lg-4 col-md-4 mb-30 grid-item">
                       <div class="video-play">
                        <img src="https://img.youtube.com/vi/c7UzRQt4KMk/mqdefault.jpg" class="img-fluid" style="width: 100%;">
                            <div class="call-to-action">
                                <a href="https://www.youtube.com/watch?v=c7UzRQt4KMk" class="view video-view"><i class="fa fa-play"></i></a>
                            </div>
                        </div>
                    </div>

                     <div class="col-xl-4 col-lg-4 col-md-4 mb-30 grid-item">
                       <div class="video-play">
                        <img src="https://img.youtube.com/vi/TViulKCUHxo/mqdefault.jpg" class="img-fluid" style="width: 100%;">
                            <div class="call-to-action">
                                <a href="https://www.youtube.com/watch?v=TViulKCUHxo" class="view video-view"><i class="fa fa-play"></i></a>
                            </div>
                        </div>
                    </div>

                     <div class="col-xl-4 col-lg-4 col-md-4 mb-30 grid-item">
                       <div class="video-play">
                        <img src="https://img.youtube.com/vi/F_VDNmncYKA/mqdefault.jpg" class="img-fluid" style="width: 100%;">
                            <div class="call-to-action">
                                <a href="https://www.youtube.com/watch?v=F_VDNmncYKA" class="view video-view"><i class="fa fa-play"></i></a>
                            </div>
                        </div>
                    </div>

                     <div class="col-xl-4 col-lg-4 col-md-4 mb-30 grid-item">
                       <div class="video-play">
                        <img src="https://img.youtube.com/vi/olvt6ifu9sY/mqdefault.jpg" class="img-fluid" style="width: 100%;">
                            <div class="call-to-action">
                                <a href="https://www.youtube.com/watch?v=olvt6ifu9sY" class="view video-view"><i class="fa fa-play"></i></a>
                            </div>
                        </div>
                    </div>

                     <div class="col-xl-4 col-lg-4 col-md-4 mb-30 grid-item">
                       <div class="video-play">
                        <img src="https://img.youtube.com/vi/iJLSHegd1Y8/mqdefault.jpg" class="img-fluid" style="width: 100%;">
                            <div class="call-to-action">
                                <a href="https://www.youtube.com/watch?v=iJLSHegd1Y8" class="view video-view"><i class="fa fa-play"></i></a>
                            </div>
                        </div>
                    </div>

                     <div class="col-xl-4 col-lg-4 col-md-4 mb-30 grid-item">
                       <div class="video-play">
                        <img src="https://img.youtube.com/vi/r_D-ZW5D_ZE/mqdefault.jpg" class="img-fluid" style="width: 100%;">
                            <div class="call-to-action">
                                <a href="https://www.youtube.com/watch?r_D-ZW5D_ZE" class="view video-view"><i class="fa fa-play"></i></a>
                            </div>
                        </div>
                    </div>

                     <div class="col-xl-4 col-lg-4 col-md-4 mb-30 grid-item">
                       <div class="video-play">
                        <img src="https://img.youtube.com/vi/xvB5NaNNNeI/mqdefault.jpg" class="img-fluid" style="width: 100%;">
                            <div class="call-to-action">
                                <a href="https://www.youtube.com/watch?v=xvB5NaNNNeI" class="view video-view"><i class="fa fa-play"></i></a>
                            </div>
                        </div>
                    </div>

                     <div class="col-xl-4 col-lg-4 col-md-4 mb-30 grid-item">
                       <div class="video-play">
                        <img src="https://img.youtube.com/vi/mQIOTG4YITQ/mqdefault.jpg" class="img-fluid" style="width: 100%;">
                            <div class="call-to-action">
                                <a href="https://www.youtube.com/watch?v=mQIOTG4YITQ" class="view video-view"><i class="fa fa-play"></i></a>
                            </div>
                        </div>
                    </div>

                     <div class="col-xl-4 col-lg-4 col-md-4 mb-30 grid-item">
                       <div class="video-play">
                        <img src="https://img.youtube.com/vi/faZ04PmFH94/mqdefault.jpg" class="img-fluid" style="width: 100%;">
                            <div class="call-to-action">
                                <a href="https://www.youtube.com/watch?v=faZ04PmFH94" class="view video-view"><i class="fa fa-play"></i></a>
                            </div>
                        </div>
                    </div>

                     <div class="col-xl-4 col-lg-4 col-md-4 mb-30 grid-item">
                       <div class="video-play">
                        <img src="https://img.youtube.com/vi/pI2rjseXBng/mqdefault.jpg" class="img-fluid" style="width: 100%;">
                            <div class="call-to-action">
                                <a href="https://www.youtube.com/watch?v=pI2rjseXBng" class="view video-view"><i class="fa fa-play"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-4 col-lg-4 col-md-4 mb-30 grid-item">
                       <div class="video-play">
                        <img src="https://img.youtube.com/vi/uNKDD1nxEvY/mqdefault.jpg" class="img-fluid" style="width: 100%;">
                            <div class="call-to-action">
                                <a href="https://www.youtube.com/watch?v=uNKDD1nxEvY" class="view video-view"><i class="fa fa-play"></i></a>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
       </section>
</main>

<style>
    .video-play:hover .view i {
    opacity: 1;
}/*.video-play:hover {
    opacity: 0.7;
    border: 3px solid #004891c7;
}*/
.video-play {
    border: 3px solid #e52f48b8;
}
</style>
@endsection